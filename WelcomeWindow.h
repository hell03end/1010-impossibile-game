#ifndef WELCOMEWINDOW_H
#define WELCOMEWINDOW_H

#include "ErrMessages.h"
#include <QWidget>
#include <QMessageBox>

namespace Ui {
    class WelcomeWindow; //why the fuck is done in such way?
}

class WelcomeWindow : public QWidget {
    Q_OBJECT
public:
    explicit WelcomeWindow(QWidget* parent = NULL);
    virtual ~WelcomeWindow(void);

    void signed_in(int code, char key = '\0'); //check sign-in' process result
    void signed_up(int code, char key = '\0'); //check registration' process result
    void set_geometry(const QRect& size); //save geometry of last opened window

private:
    Ui::WelcomeWindow* ui; //GUI for welcome window

    void sign_up(void); //try to create new user with such data
    inline void sign_in(void); //try to sign in with login and password
    inline void call_about(void); //call window with info
    inline void play_online(bool online = false); //go to mainwindow

    /* navigation */
    void go_to_registration(void); //+call_about()
    void go_to_welcome(void);
    inline void restart(void); //clear all fields and go to welcome
    inline void button_enable_sign_in(void); //check all fields needed to enable sign_in button
    inline void button_enable_register(void); //check all fields needed to enable register button
    void show_message(char type, const std::string& title,
                      const std::string& body = "", const std::string& detals = "");

signals:
    void PlayOnline(bool); //return game status
    void SignIn(const std::string& login, const std::string& password); //sent user credentials
    void SignUp(const std::string& login, const std::string& password,
                const std::string& email); //sent credentials for new user
    void CallAbout(void); //show info window
    void Geometry(const QRect&); //return geometry of window
};

#endif // WELCOMEWINDOW_H
