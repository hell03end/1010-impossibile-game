#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "Tetris.inl"
#include <QDialog>
#include <QColor>
#include <QMessageBox>

namespace Ui {
    class GameWindow;
}

class GameWindow : public QDialog {
    Q_OBJECT
public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();

    void play(unsigned long long int best_score);

private:
    Ui::GameWindow* ui;
    QColor m_colors[10];
    Tetris m_tetris;
    unsigned long long int m_best_score;
    bool m_info_was_shown;

    inline void save_score(void);
    inline void set_score(void);
    inline void go_back_to_menu(void);

    void move(int x, int y);
    void switch_elements();
    void display_element();
    void display_field();
    void display_cell(int row, int column, const QColor& color);
    void highlight(int row, int column);
    void game_over();

    void show_message(char type, const std::string& title,
                      const std::string& body = "", const std::string& detals = "");
signals:
    void GoBack(void);
    void SaveScore(unsigned long long int);
    void CallAbout(void);
};

#endif // GAMEWINDOW_H
