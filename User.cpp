#include "User.h"

User::User(const std::string& login,
           const std::string& password) {
    if (!set_login(login)) {
        m_login = DEFLOG;
        set_error(WRGDAT);
    }
    if (!set_password(password)) {
        m_password = DEFPAS;
        set_error(WRGDAT1);
    }
} //@tested

bool User::set_login(const std::string& login) {
    if (!correct_login(login)) {
        return false;
    }
    m_login = str2lower(login);

    return true;
} //@tested

const std::string& User::get_login(void) const {
    return m_login;
} //@tested

bool User::set_password(const std::string& password) {
    if (!correct_password(password)) {
        return false;
    }
    m_password = password; //crypt password with key
    return true;
} //@tested

const std::string& User::get_password() const {
    return m_password;
} //@tested

bool User::operator==(const User& user) const {
    return m_login == user.m_login && m_password == user.m_password;
} //@tested

std::string User::serialize() const {
    return sha512(xor_crypt(crypt(static_cast<std::string>(DIVSTR)+"\n"+m_login+"\n"+static_cast<std::string>(DIVSTR), m_password)));
} //@tested

bool User::deserialize(const std::string& message) {
    std::string data = decrypt(xor_crypt(sha512(message)), m_password);
    std::string div = static_cast<std::string>(DIVSTR);
    if (!checksum(data)) {
        return false;
    }
    std::string new_login = "";
    for (int i = div.size()+1; i < data.size()-div.size()-1; ++i) {
        new_login += data[i];
    }
    set_login(new_login);
    return true;
} //@tested

bool User::correct_login(const std::string& login) {
    /* check length */
    if (login.size() < MINLOG || login.size() > MAXLOG) {
        return false;
    }
    /* check symbols */
    for (auto c : login) {
        if (iscntrl(c) || isspace(c)) {
            return false;
        }
    }

    return true;
} //@tested

bool User::correct_password(const std::string& password) {
    /* check length */
    if (password.size() < MINPAS || password.size() > MAXPAS) {
        return false;
    }
    /* check symbols */
    for (auto c : password) {
        if (iscntrl(c)) {
            return false;
        }
    }
    /* check complexity */
    bool digit = false;
    bool alpha = false;
    for (auto c : password) {
        if (isdigit(c) || ispunct(c)) {
            digit = true;
        } else if (isalpha(c)) {
            alpha = true;
        }
        if (alpha && digit) {
            break;
        }
    }
    return (digit && alpha);
} //@tested

std::string User::str2lower(const std::string& login) {
    std::string result = "";

    for (auto c : login) {
        int tmp = tolower(c);
        result += static_cast<char>(tmp);
    }

    return result;
} //@tested

//@undone
std::string User::crypt(const std::string& message, const std::string& password) {
//        std::string hashed_password = add_hash(password);
//        std::reverse(hashed_password.begin(), hashed_password.end());
//        if (password.size() > message.size()) {
//            for (int i = message.size()-1; i < password.size(); ++i) {
//                hashed_password[message.size()-i-1] ^= hashed_password[i];
//            }
//        }
    std::string result = message;
    std::reverse(result.begin(), result.end());
//        for (int i = 0, j = 0; i < message.size(); ++i, j += (j < password.size()-1) ? 1 : -(password.size())) {
//            result[i] ^= hashed_password[j];
//        }
    return base64_encode(result);
} //@tested

//@undone
std::string User::decrypt(const std::string& message, const std::string& password) {
//        std::string hashed_password = add_hash(password);
//        std::reverse(hashed_password.begin(), hashed_password.end());
    std::string result = base64_decode(message);
//        if (password.size() > result.size()) {
//            for (int i = result.size()-1; i < password.size(); ++i) {
//                hashed_password[result.size()-i-1] ^= hashed_password[i];
//            }
//        }
//        for (int i = 0, j = 0; i < result.size(); ++i, j += (j < password.size()-1) ? 1 : -(password.size())) {
//            result[i] ^= hashed_password[j];
//        }
    std::reverse(result.begin(), result.end());
    return result;
} //@tested

std::string User::xor_crypt(const std::__cxx11::string &message) const {
    std::string password = "";
    std::string result = "";
    if (message.size() < m_password.size()) {
        for (int i = 0; i < message.size(); ++i) {
            password += m_password[i];
        }
        int n = password.size();
        for (int i = n, j = 0; i < m_password.size(); ++i, j = (j < message.size() ? j+1 : 0)) {
            password[j] += m_password[i];
        }
    } else {
        password = m_password;
    }
    int i = 0;
    for (auto c : message) {
        result += c ^ password[i];
        i = (i < password.size()) ? i+1 : 0;
    }
    return result;
}

std::string User::add_hash(const std::string& message) {
    char hash = 0;
    for (auto i : message) {
        hash ^= i;
    }
    std::string result = message;
    for (int i = 0; i < message.size(); ++i) {
        result[i] ^= hash;
    }
    return result;
}

std::string User::base64_encode(const std::string& message) {
    /* Copyright (C) 2004-2008 René Nyffenegger rene.nyffenegger@adp-gmbh.ch */
    const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    std::string result;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];
    int i = 0;
    unsigned int in_len = message.size();
    const unsigned char* bytes_to_encode = reinterpret_cast<const unsigned char*>(message.c_str());

    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            i = 0;
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for (int j = 0; j < 4; ++j) {
                result += base64_chars[char_array_4[j]];
            }
        }
    }

    if (i) {
        for (int j = i; j < 3; ++j) {
            char_array_3[j] = '\0';
        }

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (int j = 0; j < i+1; ++j) {
            result += base64_chars[char_array_4[j]];
        }

        while (i++ < 3) {
            result += '=';
        }
    }
    return result;
}

std::string User::base64_decode(const std::string& message) {
    /* Copyright (C) 2004-2008 René Nyffenegger rene.nyffenegger@adp-gmbh.ch */
    const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    std::string result;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];
    int i = 0;
    int in_ = 0;
    int in_len = message.size();

    while (in_len-- && message[in_] != '=' && (isalnum(message[in_]) || (message[in_] == '+') || (message[in_] == '/'))) {
        char_array_4[i++] = message[in_++];
        if (i == 4) {
            i = 0;
            for (int j = 0; j < 4; ++j) {
                char_array_4[j] = base64_chars.find(char_array_4[j]);
            }

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (int j = 0; j < 3; ++j) {
                result += char_array_3[j];
            }
        }
    }

    if (i) {
        for (int j = i; j < 4; ++j) {
            char_array_4[j] = 0;
        }
        for (int j = 0; j < 4; ++j) {
            char_array_4[j] = base64_chars.find(char_array_4[j]);
        }

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        for (int j = 0; j < i-1; ++j) {
            result += char_array_3[j];
        }
    }

    return result;
}

bool User::checksum(const std::string& message, const std::string& div) {
    if (message.size() < 2*(div.size()+1)+MINLOG || message[div.size()] != '\n' || message[message.size()-div.size()-1] != '\n') {
        return false;
    }
    for (int i = 0, j = message.size()-div.size(); i < div.size(); ++i) {
        if (div[i] != message[i] || div[i] != message[j+i]) {
            return false;
        }
    }
    return true;
}

//    bool User::set_name(const std::string& name) {
//        if (!correct_name(name)) {
//            return false;
//        }
//        m_name = to_standard_name(name);
//
//        return true;
//    }
//    const std::string& User::get_name(void) const {
//        return m_name;
//    }
//    bool User::set_surname(const std::string& surname) {
//        if (!correct_name(surname)) {
//            return false;
//        }
//        m_surname = to_standard_name(surname);
//
//        return true;
//    }
//    const std::string& User::get_surname(void) const {
//        return m_surname;
//    }
//    bool User::correct_name(const std::string& name) {
//        /* check length */
//        if (name.size() < MINNAM || name.size() > MAXNAM) {
//            return false;
//        }
//        bool space = false;
//        for (auto c : name) {
//            if (iscntrl(c) || ispunct(c) || isdigit(c)) {
//                return false;
//            } else if (isspace(c) && !space) {
//                space = true;
//            } else if (isspace(c) && space) {
//                return false; //only one space is available
//            }
//        }
//
//        return !(space && name.size() < 2*MINNAM); //name with space should be a bit longer
//    }
