/**
 * @file User.h
 * @memberof authication system
 * @category account
 *
 * @brief Class of simple user.
 *
 * @details
 * Contains login/password and methods to manipulate them.
 * To configure min/max valid lengths of login/password redefine "LENBRD".
 * To crypt password simple xor-method + base64 is used.
 *
 * @author hell03end
 * @version 0.2.2
 * @date 04-Dec-16
 */

#ifndef USER_H
#define USER_H

#ifndef DEFUSR //default data for User if they aren't set manually
#define DEFLOG "no_login" //default login
#define DEFPAS "dick123!!!" //default password
#endif //DEFUSR

#ifndef LENBRDR
#define MINLOG 3
#define MINPAS 7
#define MAXLOG 20
#define MAXPAS 30
#endif //LENBRDR

#define DIVSTR "=#!\tes\t!#="

#include "BaseObject.inl"
#include "sha512.h"
#include <algorithm>
#include <cctype>

class User : public BaseObject<std::string> {
public:
    User(const std::string& login = DEFLOG, const std::string& password = DEFPAS);
    virtual ~User(void) {/* empty */}

    virtual bool set_login(const std::string& login); //check login to be correct, then assign in to m_login
    const std::string& get_login(void) const;
    virtual bool set_password(const std::string& password);
    const std::string& get_password(void) const; //get password if key is right (+Ex:err)

    virtual bool operator==(const User& user) const;
    virtual std::string serialize(void) const; //transform object data into standard representation type
    virtual bool deserialize(const std::string& message); //create object from standard representation type

    static bool correct_login(const std::string& login); //check login format is correct
    static bool correct_password(const std::string& password); //check password format is correct
    static std::string str2lower(const std::string& login); //get login suit standards
    static std::string crypt(const std::string& message, const std::string& key);
    static std::string decrypt(const std::string& message, const std::string& key);
protected:
    std::string m_login;
    std::string m_password;

    std::string xor_crypt(const std::string& message) const;
    static std::string add_hash(const std::string& message);
    static std::string base64_encode(const std::string& message);
    static std::string base64_decode(const std::string& message);
    static bool checksum(const std::string& message, const std::string& div = DIVSTR);
};

#endif //USER_H
