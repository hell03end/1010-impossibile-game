#include "Player.h"

Player::Player(const std::string& login,
               const std::string& email,
               const std::string& password) :
        User(login, password) {
    if (!set_email(email)) {
        m_email = DEFEML;
        set_error(WRGDAT);
    }
    m_score = 0;
} //@tested

bool Player::set_email(const std::string& email) {
    if (!correct_email(email)) {
        return false;
    }
    m_email = str2lower(email);
    return true;
} //@tested

std::string Player::get_email() const {
    return m_email;
} //@tested

void Player::set_score(const unsigned long int score) {
    m_score = score;
} //@tested

unsigned long int Player::get_score(void) const {
    return m_score;
} //@tested

unsigned long int Player::operator+(unsigned long int score) {
    return m_score += score;
} //@tested

unsigned long int Player::operator-(unsigned long int score) {
    return m_score -= score;
} //@tested

unsigned long int Player::operator*(unsigned long int score) {
    return m_score *= score;
} //@tested

bool Player::operator==(const Player& player) const {
    return m_score == player.m_score;
} //@tested

bool Player::operator==(const User& user) const {
    return this->m_login == user.get_login() &&  this->m_password == user.get_password();
} //@tested

bool Player::operator>(const Player& player) const {
    return m_score > player.m_score;
} //@tested

std::string Player::serialize() const {
    std::string div = static_cast<std::string>(DIVSTR);
    char t_score[12];
    sprintf(t_score, "%ld", m_score);
    return crypt(div+"\n"+m_login+"\n"+div+"\n"+t_score+"\n"+div+"\n"+m_email+"\n"+div, m_password);
} //@tested

bool Player::deserialize(const std::string& message) {
    std::string div = static_cast<std::string>(DIVSTR);
    std::string decrypted = decrypt(message, m_password);
    std::string login_data = "";
    std::string score_data = "";
    std::string email_data = "";

    if (decrypted.size() < div.size()*4 + MINLOG*2 + MINPAS) {
        return false;
    }

    char key = 0;
    std::string temp = "";
    for (auto i : decrypted) {
        if (i != '\n' && key%2 == 0) {
            temp += i;
        } else if (i != '\n' && key == 1) {
            login_data += i;
        } else if (i != '\n' && key == 3) {
            score_data += i;
        } else if (i != '\n' && key == 5) {
            email_data += i;
        }
        if (i == '\n') {
            key++;
            if (temp != div) {
                return false;
            }
            if (key%2 == 0) {
                temp = "";
            }
        }
    }

    set_login(login_data);
    set_email(email_data);

    char t[12];
    for (int i = 0; i < score_data.size(); ++i) {
        t[i] = score_data[i];
    }
    set_score(static_cast<unsigned long int>(atoi(t)));

    return true;
} //@tested

Player::operator User() const {
    User user(m_login, m_password);
    return user;
} //@tested

bool Player::correct_email(const std::string& email) {
    bool alpha_before_smbl = false;
    bool special_smbl = false;
    bool alpha_between_smbl = false;
    bool dot_smbl = false;
    bool alpha_after_smbl = false;

    for (auto i : email) {
        if (!dot_smbl && !special_smbl && (isalpha(i) || isdigit(i) || i == '_' || i == '-')) {
            alpha_before_smbl = true;
        } else if (!dot_smbl && alpha_before_smbl && !special_smbl && i == '@') {
            special_smbl = true;
        } else if (!dot_smbl && special_smbl && alpha_before_smbl && (isalpha(i) || isdigit(i))) {
            alpha_between_smbl = true;
        } else if (special_smbl && alpha_before_smbl && alpha_between_smbl && i == '.') {
            dot_smbl = true;
            alpha_after_smbl = false;
        } else if (special_smbl && alpha_before_smbl && alpha_between_smbl && isalpha(i)) {
            alpha_after_smbl = true;
        } else {
            return false;
        }
    }
    return alpha_before_smbl && special_smbl && alpha_between_smbl && dot_smbl && alpha_after_smbl;
//        return correct_login(email);
} //@tested

//    bool Player::operator!=(const Player& player) const {
//        return m_score != player.m_score;
//    }
//    bool Player::operator>=(const Player& player) const {
//        return m_score >= player.m_score;
//    }
//    bool Player::operator<(const Player& player) const {
//        return m_score < player.m_score;
//    }
//    bool Player::operator<=(const Player& player) const {
//        return m_score <= player.m_score;
//    }
