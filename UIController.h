#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "Player.h"
//#include "Server.h"
#include "MenuWindow.h"
#include "WelcomeWindow.h"
#include "GameWindow.h"
#include "InfoWindow.h"
#include <map>
#include <iterator>
#include <string>
#include <fstream>
#include <QObject>
#include <QDebug>
//#include <QThread>
#include <QMutex>

static bool file_exists(const std::string& path);

class UIController : public QObject {
    Q_OBJECT
public:
    explicit UIController(const std::string& path2db = "DB", QObject *parent = NULL);
    virtual ~UIController(void);

//signals:
//    void CallServerSignIn(const std::string& value);
//    void CallServerSignUp(const std::string& key, const std::string& value);
//    void CallServerUpdate(const std::string& key, const std::string& value);
//    void CallServerRemove(const std::string& key);
//    void CallServerGetPlayers(int amount = -1);

private:
    typedef std::map<std::string, std::string> Dictionary;

    bool m_online;
    MenuWindow* menu_ui; // graphical user interface for menu
    GameWindow* game_ui; // graphical user interface for gaming process
    InfoWindow* info_ui; // graphical user interface for information about game
    WelcomeWindow* welcome_ui; // graphical user interface for welcome screen
    Player m_player; // current player
    QRect m_curr_geom; // geometry of windows, except game window with fixed geometry
//    Server<int, std::string> server; // server
//    QThread server_th; // thread to run server
    Dictionary m_db;
    std::string m_path2db;
    QMutex m_mutex;

    inline void show_about(void);
    inline void remember_geom(const QRect& size);

    /* welcome window */
    inline void open_menu_from_welcome(bool);
    void user_add(const std::string& login, const std::string& password,
                  const std::string& email);
    void user_log_in(const std::string& login, const std::string& password);
    void user_logged_in(const std::string& value);

    /* menu window */
    inline void open_game_w();
    inline void reset_score(void);
    inline void user_delete(void);
    inline void open_welcome_w(void);
    void players_show();

    /* game window */
    inline void set_score(unsigned long long int score);
    inline void open_menu_from_game(void);
};

#endif // CONTROLLER_H
