#include "MenuWindow.h"
#include "ui_MenuWindow.h"

MenuWindow::MenuWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    /* buttons' actions */
    connect(ui->pushButton_exit, &QPushButton::clicked,
            this, &MenuWindow::close);
    connect(ui->pushButton_back_scores, &QPushButton::clicked,
            this, &MenuWindow::go_to_main_window);
    connect(ui->pushButton_back_settings, &QPushButton::clicked,
            this, &MenuWindow::go_to_main_window);
    connect(ui->pushButton_scores, &QPushButton::clicked,
            this, &MenuWindow::go_to_scores);
    connect(ui->pushButton_settings, &QPushButton::clicked,
            this, &MenuWindow::go_to_settings);
    connect(ui->pushButton_play, &QPushButton::clicked,
            this, &MenuWindow::play);
    connect(ui->pushButton_about, &QPushButton::clicked,
            this, &MenuWindow::call_about);
    connect(ui->pushButton_delete, &QPushButton::clicked,
            this, &MenuWindow::delete_user);
    connect(ui->pushButton_logout, &QPushButton::clicked,
            this, &MenuWindow::log_out);
    connect(ui->pushButton_rest_score, &QPushButton::clicked,
            this, &MenuWindow::rest_score);
}

MenuWindow::~MenuWindow() {
    delete ui;
}

void MenuWindow::set_offline_settings() {
    ui->pushButton_scores->setEnabled(false);
    ui->label_user_greetings->setText("Hello, User!");
    ui->pushButton_delete->setEnabled(false);
    ui->pushButton_rest_score->setEnabled(false);
    go_to_main_window();
}

void MenuWindow::set_online_settings(const std::string& login, unsigned long long int score) {
    ui->pushButton_scores->setEnabled(true);
    ui->label_user_greetings->setText("Hello, "+QString::fromStdString(login)+"!");
    ui->pushButton_delete->setEnabled(true);
    ui->pushButton_rest_score->setEnabled(true);
    ui->label_your_score->setText(QString::fromStdString(login)+" score:");
    ui->label_score->setText(QString::fromStdString(std::to_string(score)));
    go_to_main_window();
}

void MenuWindow::show_player(const std::string& login, const std::string& email,
                             unsigned long long int score) {
    ui->textEdit_scores->append(QString::fromStdString(login+"\t"+email+"\t"+std::to_string(score)));
}

void MenuWindow::clear_palyers() {
    ui->textEdit_scores->clear();
}

void MenuWindow::update_score(unsigned long long int score) {
    ui->label_score->setText(QString::fromStdString(std::to_string(score)));
}

void MenuWindow::set_geometry(const QRect& size) {
    this->setGeometry(size);
}

void MenuWindow::rest_score() {
    ui->label_score->setText(QString::fromStdString(std::to_string(0)));
    emit RestScore();
    emit ShowPlayers();
}

void MenuWindow::call_about() {
    emit CallAbout();
}

void MenuWindow::log_out() {
    emit Geometry(this->geometry());
    emit LogOut();
    this->close();
}

void MenuWindow::delete_user() {
    this->setEnabled(false);
    this->close();
    emit Geometry(this->geometry());
    emit DeleteUser();
}

void MenuWindow::play() {
    this->setEnabled(false);
    this->close();
    emit Play();
}

void MenuWindow::go_to_main_window() {
    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_main));
}

void MenuWindow::go_to_scores() {
    emit(ShowPlayers()); // <-- ???
    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_scores));
}

void MenuWindow::go_to_settings() {
    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_settings));
}

/* show window (message box) with some info */
void MenuWindow::show_message(char type, const std::__cxx11::string& title,
                              const std::__cxx11::string& body,
                              const std::__cxx11::string& detals) {
    QMessageBox window;
    if (type == 'q' || type == 'Q') {
        window.setIcon(QMessageBox::Question);
    } else if (type == 'i' || type == 'I') {
        window.setIcon(QMessageBox::Information);
    } else if (type == 'w' || type == 'W') {
        window.setIcon(QMessageBox::Warning);
    } else if (type == 'c' || type == 'C') {
        window.setIcon(QMessageBox::Critical);
    }
    window.setDefaultButton(QMessageBox::Ok); //create only one button
    window.setText("<b>"+QString::fromStdString(title)+"</b>"); //show title message
    if (body != "") {
        window.setInformativeText(QString::fromStdString(body)); //add informative text
    }
    if (detals != "") {
        window.setDetailedText(QString::fromStdString(detals)); //add detailed text
    }
    window.exec();
}
