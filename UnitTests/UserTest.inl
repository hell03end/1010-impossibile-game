/**
 * @file UnitTest/UserTest.inl
 * @memberof unit tests
 * @category implementation of test
 * @namespace UnitTest
 * @inherit CommonTest
 *
 * @author hell03end
 */

#ifndef USER_UNIT_TEST_INL
#define USER_UNIT_TEST_INL

#include "CommonTest.inl"
#include "../User.h"

namespace UnitTest {
    class UserTest : public CommonTest {
    public:
        UserTest(unsigned long long int test_memory = 0) : CommonTest(test_memory) {  };
        virtual ~UserTest(void) { /* nothing */ };
    private:
        static std::string tData;
        static std::string tData1;

        void do_tests(void);
    };

    std::string UserTest::tData = "Alex";
    std::string UserTest::tData1 = " _d*os_ ";

    void UserTest::do_tests() {
        report_error((User::str2lower("D*S !") == "d*s !"), "str2lower");
        report_error((User::crypt("Hello, world!", tData1) !=
                      User::decrypt(User::crypt("Hello, world!", tData1), tData1)), "crypto methods (long)");
        report_error((User::crypt(tData, tData1) !=
                      User::decrypt(User::crypt(tData, tData1), tData1)), "crypto methods (short)");

        report_error((User::correct_login("her")), "correct_login() (normal)");
        report_error((!User::correct_login("hi")), "correct_login() (short)");
        report_error((!User::correct_login("   ")), "correct_login() (empty)");
        report_error((!User::correct_login("asdfghjklzxcvbnmqwertyuiop")), "correct_login() (long)");

        report_error((User::correct_password("pass123")), "correct_password() (normal)");
        report_error((User::correct_password("password!")), "correct_password() (normal with symbol)");
        report_error((!User::correct_password("password")), "correct_password() (weak, without numbers)");
        report_error((!User::correct_password("1234321")), "correct_password() (weak, without alphas)");
        report_error((!User::correct_password("abs12")), "correct_password() (short)");
        report_error((!User::correct_password("asdfghjklzxcvbnmqwertyuioppqowieurytthd")), "correct_password() (long)");

        User user;
        report_error((user.get_login() == DEFLOG && user.get_password() == DEFPAS && !user.get_err_amount()), "Standard constructor");

        report_error((user.set_login(tData) && user.get_login() == User::str2lower(tData)), "set_login() (normal)");
        report_error((!user.set_login(tData1) && user.get_login() == User::str2lower(tData)), "set_login() (wrong)");

        report_error((user.set_password(tData1) && user.get_password() == tData1), "set_password() (normal)");
        report_error((!user.set_password(tData) && user.get_password() == tData1), "set_password() (wrong)");

        try {
            User* user1 = new User();
            *user1 = user;
            report_error((user1->get_login() == User::str2lower(tData) && user1->get_password() == tData1), "Operator=");
            report_error((user == *user1), "Operator==");
            delete user1;

            user1 = new User(user);
            report_error((user1->get_login() == User::str2lower(tData) && user1->get_password() == tData1), "Coping constructor");
            delete user1;

            user1 = new User(tData, tData1);
            report_error((user1->get_login() == User::str2lower(tData) && user1->get_password() == tData1), "Initialization constructor");
        } catch (int err) {
            if (err != ACCDEN) {
                show_err(err);
                report_error((false), "Exception", "was raised");
            }
        }

        user.set_login(tData);
        User user2;
        user2.set_password(user.get_password());
        report_error((user2.deserialize(user.serialize()) && user == user2), "serialization process");

        if (m_test_memory) {
            m_time = clock();
            /* test for memory leak */
            for (int i = 0; i < m_test_memory; i++) {
                User* user1 = new User();
                delete user1;
            }
            m_time = clock() - m_time;
        }
    }
}

#endif //USER_UNIT_TEST_INL
