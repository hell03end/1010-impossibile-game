/**
 * @file UnitTest/CommonTest.inl
 * @memberof unit tests
 * @category CommonTest, base class for testing
 * @namespace UnitTest
 *
 * @brief Abstract class which implements declare basic structure for unit tests.
 *
 * @details
 * Use "UnitTest::CommonTest* obj = new *NameTest*()" to initialize test.
 * Use "UnitTest::CommonTest::obj.test(int ERRNO)" to start testing.
 * Use "UnitTest::CommonTest::obj.report_error(bool, -string, -string)" to check whether code work normally.
 * Use "UnitTest::CommonTest::obj.show_error(int ERRNO)" to show message of error with known type.
 * Field "m_success" is used to indicate test status: (not)pass.
 * Field "test_memory" is used to turn off/on tests of memory leaks, which usually long.
 * Field "m_time" is used to show time of memory tests.
 * Each class, which hair of this class, should realize "do_test()" private method.
 *
 * @author hell03end
 * @version 2.2
 * @date 01-Dec-16 13:16
 */

#ifndef COMMON_UNIT_TEST_INL
#define COMMON_UNIT_TEST_INL

#include "../ErrMessages.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

namespace UnitTest {

    class CommonTest {
    public:
        CommonTest(unsigned long long int test_memory = 0); ///initial values
        virtual ~CommonTest(void);

        /// \param error_number
        void test(int error_number); ///test entering point
    protected:
        time_t m_time_start; ///time of starting test
        time_t m_time;
        bool m_success; ///result of testing
        unsigned long long int m_test_memory; ///do tests for memory leaks (needs many time)

        /// \brief abstract function, contains tests themselves
        virtual void do_tests(void) = 0;
        /// \brief sent error message to stderr and change test status to false
        /// \param a_assumption, a_message, a_conclusion
        void report_error(bool a_assumption, const std::string& a_message = "Something",
                          const std::string& a_conclusion = "doesn't work");
        /// \brief send message about known types of errors to stderr \param errnum
        static void show_err(int errnum);
    };

    CommonTest::CommonTest(unsigned long long int test_memory) {
        m_time_start = clock();
        m_time = 0;
        this->m_success = true;
        m_test_memory = test_memory;
    }

    CommonTest::~CommonTest() {}

    void CommonTest::test(int error_number) {
        m_time_start = clock(); //remember when started
        do_tests(); //all testing process is here
        /* show time of testing */
        std::cerr << "Test " << error_number << " time: " << clock() - m_time_start << std::endl;
        if (m_test_memory && m_time != 0) {
            std::cerr << "\tMemory test time is: " << m_time << std::endl;
        }
        if (!m_success) {
            std::cin.get(); //pause
            if (error_number) {
                exit(error_number); //exit with error code
            } else {
                srand(static_cast<unsigned int>(time(0)));
                for (int errcode = rand(); ; errcode = rand()) {
                    if (errcode != 0) {
                        exit(errcode); //exit with random error code
                    }
                }
            }
        }
    }

    inline void CommonTest::report_error(bool a_assumption, const std::string &a_message, const std::string& a_conclusion) {
        if (!a_assumption) {
            m_success = false; //test fault
            std::cerr << "Error:\t" << a_message << " " << a_conclusion << "!\n";
        }
    }

    void CommonTest::show_err(int errnum) {
        std::string message = "Unknown error type"; //if no such error code will be found

        /* choose what to print */
        if (errnum == EXIT_SUCCESS) {
            return;
        } else if (errnum == PTRNLL) {
            message = "NULL-pointer dereference";
        } else if (errnum == NALLOC1) {
            message = "Can't allocate memory (1)";
        } else if (errnum == NALLOC) {
            message = "Can't allocate memory";
        } else if (errnum == INVPOS) {
            message = "Invalid position";
        } else if (errnum == POSOUT) {
            message = "Position out of container' bounds";
        } else if (errnum == EMPVAL) {
            message = "No elements in container";
        } else if (errnum == ACCDEN) {
            message = "ACCESS DENIED";
        } else if (errnum == WRGDAT) {
            message = "Can't assign: wrong format of data";
        } else if (errnum == WRGDAT1) {
            message = "Can't assign: wrong format of data (1)";
        } else if (errnum == NFOUND) {
            message = "Element wasn't found";
        }

        std::cerr << "Error: " << message << ".\n";
    }
}

#endif //COMMON_UNIT_TEST_INL
