/**
 * @file UnitTest/ListTestObject.inl
 * @memberof unit tests
 * @category implementation of test
 * @namespace UnitTest
 * @inherit from CommonTest
 *
 * @author hell03end
 */

#ifndef LIST_UNIT_TEST_OBJECT_INL
#define LIST_UNIT_TEST_OBJECT_INL

#include "CommonTest.inl"
#include "../List.inl"
#include <string>

namespace UnitTest {

    class ListTestObject : public CommonTest {
    public:
        ListTestObject(unsigned long long int test_memory = 0) {  };
        virtual ~ListTestObject(void) { /* Nothing */ };
    private:
        static std::string tDATA; //data to test
        static int tSIZE; //size of arrays or etc. to test

        inline void do_tests(void);
        void do_test_number(unsigned int number);
    };

    std::string ListTestObject::tDATA = static_cast<std::string>("Hello, world!\n");
    int ListTestObject::tSIZE = 100;

    void ListTestObject::do_tests() {
        for (int i = 0; i < 7; ++i) {
            do_test_number(i);
        }
        if (m_test_memory) {
            List<std::string> list;
            m_time = clock();
            /* test for memory leak */
            for (int i = 0; i < m_test_memory; i++) {
                report_error((list.append(tDATA) && list.size() == 1), "Adding in cycle(memory leak test)");
                report_error((list.remove_all_same(tDATA) && list.empty()), "Removing in cycle(memory leak test)");
            }
            m_time = clock() - m_time;
        }
    }

    void ListTestObject::do_test_number(unsigned int number) {
        List<std::string> list;
        report_error((list.empty() && list.size() == 0), "Standard constructor or empty()/size() methods");

        if (number == 0) {
            try {
                List<std::string>* list1 = new List<std::string>(list);
                report_error((list1->empty() && list1->size() == 0), "Coping constructor (empty list)");
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in coping constructor");
            }
        } else if (number == 1) {
            try {
                std::string array[tSIZE];
                for (int i = 0; i < tSIZE; ++i) {
                    array[i] = i;
                }

                List<std::string>* list1 = new List<std::string>(tSIZE, array);
                report_error((!list1->empty() && list1->size() == tSIZE), "Constructor of initialization");

                for (int i = 0; i < tSIZE; ++i) {
                    std::string temp = (*list1)[i];
                    report_error((temp == array[i]), "Direct access operator");
                }
                for (int i = tSIZE; i; --i) {
                    std::string temp = (*list1)[-i];
                    report_error((temp == array[tSIZE-i]), "Direct access operator (reversed)");
                }
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in constructor of initialization");
            }
        } else if (number == 2) {
            report_error((list.append(tDATA) && !list.empty() && list.size() == 1), "Addition");
            try {
                List<std::string>* list1 = new List<std::string>(list);
                report_error((!list1->empty() && list1->size() == 1), "Coping constructor");
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in coping constructor");
            }
        } else if (number == 3) {
            for (int i = 0; i < tSIZE/10; ++i) {
                report_error((list + tDATA && !list.empty() && list.size() == i + 1), "Operator+ (in cycle)");
            }
            report_error((list - tDATA && list.empty() && list.size() == 0), "Removing all same objects");
        } else if (number == 4) {
            for (int i = 0; i < tSIZE; ++i) {
                report_error(((!list.insert(0, tDATA)) && !list.empty() && list.size() == i + 1), "Inserting (in cycle)");
            }
            for (int i = tSIZE/2; i; --i) {
                if (char err = list.remove(i)) {
                    show_err(err);
                    report_error((false), "Exception", "in removing by index");
                }
                std::string a = tDATA;
                report_error((list.size() != i + tSIZE/2), "Removing by index (in cycle)");
            }
            list.clear();
            report_error((list.empty()), "Clearing");
        } else if (number == 5) {
            for (int i = 0; i < tSIZE; ++i) {
                list.append(tDATA);
            }
            list([](std::string* element){ element->erase(); });
            for (int i = 0; i < tSIZE; ++i) {
                report_error((list[i].empty()), "Functor");
            }
        } else if (number == 6) {
            try {
                std::string a = static_cast<std::string>("C++!\n");
                std::string array[tSIZE];
                for (int i = 0; i < tSIZE; ++i) {
                    array[i] = tDATA;
                }
                List<std::string>* list1 = new List<std::string>(tSIZE, array);
                array[tSIZE-1] = a;
                List<std::string>* list2 = new List<std::string>(tSIZE, array);
                report_error(!(*list1 == *list2), "Operator==");
                report_error((*list1 != *list2), "Operator!=");
                report_error((*list1 > *list2), "Operator>");
                report_error((*list1 >= *list2), "Operator>=");
                report_error(!(*list1 < *list2), "Operator<");
                report_error(!(*list1 <= *list2), "Operator<=");

                (*list1) + (*list2);
                report_error((list1->size() == tSIZE*2), "Operator+(list)");

                list1 = list2;
                report_error(((*list1)[tSIZE-1] == (*list2)[tSIZE-1]), "Operator=");
            } catch (...) {
                std::cerr << "Something wrong, I can feel it...";
            }
        }
    }

}

#endif //LIST_UNIT_TEST_OBJECT_INL
