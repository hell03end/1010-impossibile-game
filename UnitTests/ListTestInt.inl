/**
 * @file UnitTest/ListTestInt.inl
 * @memberof unit tests
 * @category implementation of test
 * @namespace UnitTest
 * @inherit from CommonTest
 *
 * @author hell03end
 */

#ifndef LIST_UNIT_TEST_INT_INL
#define LIST_UNIT_TEST_INT_INL

#include "CommonTest.inl"
#include "../List.inl"

namespace UnitTest {

    class ListTestInt : public CommonTest {
    public:
        ListTestInt(unsigned long long int test_memory = 0) {  };
        virtual ~ListTestInt(void) { /* nothing */ };
    private:
        static int tDATA; //data to test
        static int tSIZE; //size of arrays or etc. to test

        inline void do_tests(void);
        void do_test_number(unsigned int number);
    };

    int ListTestInt::tDATA = 1;
    int ListTestInt::tSIZE = 100;

    void ListTestInt::do_tests() {
        for (int i = 0; i < 9; ++i) {
            do_test_number(i);
        }
        if (m_test_memory) {
            List<int> list;
            m_time = clock();
            /* test for memory leak */
            for (int i = 0; i < m_test_memory; i++) {
                report_error((list.append(tDATA) && list.size() == 1), "Adding in cycle(memory leak test)");
                report_error((list.remove_all_same(tDATA) && list.empty()), "Removing in cycle(memory leak test)");
            }
            m_time = clock() - m_time;
        }
    }

    void ListTestInt::do_test_number(unsigned int number) {
        List<int> list;
        report_error((list.empty() && list.size() == 0), "Standard constructor or empty()/size() methods");

        if (number == 0) {
            try {
                List<int>* list1 = new List<int>(list);
                report_error((list1->empty() && list1->size() == 0), "Coping constructor (empty list)");
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in coping constructor");
            }
        } else if (number == 1) {
            try {
                int array[tSIZE];
                for (int i = 0; i < tSIZE; ++i) {
                    array[i] = i;
                }

                List<int>* list1 = new List<int>(tSIZE, array);
                report_error((!list1->empty() && list1->size() == tSIZE), "Constructor of initialization");

                for (int i = 0; i < tSIZE; ++i) {
                    int temp = (*list1)[i];
                    report_error((temp == array[i]), "Direct access operator");
                }
                for (int i = tSIZE; i; --i) {
                    int temp = (*list1)[-i];
                    report_error((temp == array[tSIZE-i]), "Direct access operator (reversed)");
                }

                list.insert(0, 1);
                report_error((list[0] == 1), "Inserting in test 1");
                list[0] = 2;
                report_error((list[0] == 2), "Change value throw direct access operator");
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in constructor of initialization");
            }
        } else if (number == 2) {
            report_error((list.append(tDATA) && !list.empty() && list.size() == 1), "Addition");
            try {
                List<int>* list1 = new List<int>(list);
                report_error((!list1->empty() && list1->size() == 1), "Coping constructor");
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception", "in coping constructor");
            }
        } else if (number == 3) {
            for (int i = 0; i < tSIZE/10; ++i) {
                report_error((list + tDATA && !list.empty() && list.size() == i + 1), "Operator+ (in cycle)");
            }
            report_error((list - tDATA && list.empty() && list.size() == 0), "Removing all same objects");
        } else if (number == 4) {
            for (int i = 0; i < tSIZE; ++i) {
                report_error((!list.insert(0, i) && !list.empty() && list.size() == i + 1), "Inserting (in cycle)");
            }
            for (int i = tSIZE/2; i; --i) {
                if (char err = list.remove(i)) {
                    show_err(err);
                    report_error((false), "Exception", "in removing by index");
                }
                report_error((list.size() != i + tSIZE/2 && list[i] == 48), "Removing by index (in cycle)");
            }
            list.clear();
            report_error((list.empty()), "Clearing");
        } else if (number == 5) {
            for (int i = 0; i < tSIZE; ++i) {
                list.append(i);
            }
            list([](int* element){ *element *= 2; });
            for (int i = 0; i < tSIZE; ++i) {
                report_error((list[i] == i*2), "Functor");
            }
        } else if (number == 6) {
            try {
                int array[tSIZE];
                for (int i = 0; i < tSIZE; ++i) {
                    array[i] = i;
                }
                List<int>* list1 = new List<int>(tSIZE, array);
                array[tSIZE-1] = -tDATA;
                List<int>* list2 = new List<int>(tSIZE, array);
                report_error(!(*list1 == *list2), "Operator==");
                report_error((*list1 != *list2), "Operator!=");
                report_error((*list1 > *list2), "Operator>");
                report_error((*list1 >= *list2), "Operator>=");
                report_error(!(*list1 < *list2), "Operator<");
                report_error(!(*list1 <= *list2), "Operator<=");

                (*list1) + (*list2);
                report_error((list1->size() == tSIZE*2), "Operator+(list)");

                list1 = list2;
                report_error(((*list1)[tSIZE-1] == (*list2)[tSIZE-1]), "Operator=");
            } catch (int err) {
                show_err(err);
                std::cerr << "Something wrong, I can feel it...";
            }
        } else if (number == 7) {
            for (int i = 0; i < tSIZE/10; ++i) {
                list.append(i+1);
            }
            try {
                list.sort([](const int& obj1, const int& obj2) { return obj2 > obj1; });
                for (int i = 0; i < list.size(); ++i) {
                    report_error((list[i] == tSIZE/10-i), "Sorting list");
                }
            } catch (int err) {
                show_err(err);
                report_error((false), "Exception was raised", "in sorting");
            }
        } else if (number == 8) {
            for (int i = 0; i < 10; ++i) {
                list.append(i);
            }
            for (int i = 0; i < 10; ++i) {
                report_error((list.find(i, [](const int& obj, const int& el) { return obj == el; }) == i), "find()");
            }
        }
    }

}

#endif //LIST_UNIT_TEST_INT_INL
