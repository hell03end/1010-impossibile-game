/**
 * @file UnitTest/PlayerTest.inl
 * @memberof unit tests
 * @category implementation of test
 * @namespace UnitTest
 * @inherit CommonTest
 *
 * @author hell03end
 */

#ifndef PLAYER_UNIT_TEST_INL
#define PLAYER_UNIT_TEST_INL

#include "CommonTest.inl"
#include "UserTest.inl"
#include "../Player.h"

namespace UnitTest {

    class PlayerTest : public CommonTest {
    public:
        PlayerTest(unsigned long long int test_memory = 0) : CommonTest(test_memory) {  };
        virtual ~PlayerTest(void) { /* Nothing */ };
    private:
        static std::string tData;
        static std::string tData1;
        static int tData2;

        void do_tests(void);
    };

    std::string PlayerTest::tData = "her_1@hse.ru";
    std::string PlayerTest::tData1 = " _d*os_ ";
    int PlayerTest::tData2 = 100;

    void PlayerTest::do_tests() {
        /* test class of parent */
        CommonTest* user_test = new UserTest();
        user_test->test(EXIT_FAILURE);

        report_error((Player::correct_email("her_1@hse.ru")), "correct_email() (normal)");
        report_error((Player::correct_email("her_2@edu.hse.ru")), "correct_email() (normal double dot)");
        report_error((!Player::correct_email("her.1@edu.ru")), "correct_email() (wrong smbl)");
        report_error((!Player::correct_email("her2@e_du.ru")), "correct_email() (wrong smbl2)");
        report_error((!Player::correct_email("her3@e_du.")), "correct_email() (wrong ending)");
        report_error((!Player::correct_email("her4.edu.ru")), "correct_email() (wrong format @)");
        report_error((!Player::correct_email("her5@edu")), "correct_email() (wrong format .)");

        Player player;
        report_error((player.get_score() == 0 && player.get_email() == DEFEML), "Standard constructor");

        Player* player1 = new Player(player.get_login(), player.get_password());
        report_error((player1->get_score() == 0 && player1->get_email() == DEFEML), "Constructor of initialization");
        report_error((player == *player1), "operator==");
        delete player1;
        player1 = new Player(player);
        report_error((player1->get_score() == 0 && player1->get_email() == DEFEML), "Constructor of coping");
        delete player1;

        report_error((player.set_email(tData) && player.get_email() == tData), "set_nickname() (correct)");
        report_error((!player.set_email("her5@edu") && player.get_email() == tData), "set_nickname() (incorrect)");

        player.set_score(tData2);
        report_error((player.get_score() == tData2), "set_score()");

        player1 = new Player();
        report_error(!(*player1 == player), "operator!=");
        *player1 = player;
        report_error((*player1 == player), "operator=");

        player.set_score(tData2);
        player1->set_score(tData2*2);
        report_error((*player1 > player), "operator>");
        delete player1;

        player.set_score(0);
        report_error(((player + tData2) == tData2), "operator+(int)");
        report_error(((player - tData2) == 0), "operator-(int)");
        player.set_score(tData2/10);
        report_error((player * 10 == tData2), "operator*(int)");

        player.set_login(tData);
        player.set_score(1000);
        Player player2;
        player2.set_password(player.get_password());
        report_error((player2.deserialize(player.serialize()) && player == player2), "serialization process");

        User user = User(player);
        report_error((user.get_login() == player.get_login() &&
                     user.get_password() == player.get_password()), "type cast");
        report_error((player == user), "operator==(User)");

        if (m_test_memory) {
            m_time = clock();
            /* test for memory leak */
            for (int i = 0; i < m_test_memory; i++) {
                Player* player1 = new Player();
                delete player1;
            }
            m_time = clock() - m_time;
        }
    }
}

#endif //PLAYER_UNIT_TEST_INL
