/**
 * @file UnitTest/UserTests.inl
 * @memberof unit tests
 * @category implementation of test thread
 * @namespace UnitTest
 *
 * @author hell03end
 */

#ifndef UNITTESTS_H
#define UNITTESTS_H

#include "UserTest.inl"
#include "PlayerTest.inl"
#include <QObject>

#ifndef TEST_MEMORY
#define TEST_MEMORY 1000
#endif //TEST_MEMORY

class UnitTests : public QObject {
    Q_OBJECT
public:
    explicit UnitTests(QObject *parent = NULL) : QObject(parent) { /* nothing */ }
    virtual ~UnitTests(void) { /* nothing */ }

    void run(void) {
        UnitTest::CommonTest* test;

        test = new UnitTest::UserTest(TEST_MEMORY);
        test->test(1);
        delete test;

        test = new UnitTest::PlayerTest(TEST_MEMORY);
        test->test(2);
        delete test;

        emit Finished(true);
    }

signals:
    void Finished(bool); ///signal about ending tests

private:
    //
};

#endif // UNITTESTS_H
