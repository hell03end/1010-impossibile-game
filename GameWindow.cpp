#include "GameWindow.h"
#include "ui_GameWindow.h"

GameWindow::GameWindow(QWidget *parent) : QDialog(parent), ui(new Ui::GameWindow) {
    ui->setupUi(this);
    m_info_was_shown = false;

    m_colors[0].setRgb(0,120,215); // blue
    m_colors[1].setRgb(0,153,188);
    m_colors[2].setRgb(0,178,148); // green
    m_colors[3].setRgb(16,124,16);
    m_colors[4].setRgb(73,130,5);
    m_colors[5].setRgb(255,185,0); // yellow
    m_colors[6].setRgb(255,140,0);
    m_colors[7].setRgb(247,99,12);
    m_colors[8].setRgb(255,67,67);
    m_colors[9].setRgb(232,17,35); // red

    ui->tableWidget_field->setRowCount(10);
    ui->tableWidget_field->setColumnCount(10);

    connect(ui->pushButton_back, &QPushButton::clicked,
            this, &GameWindow::go_back_to_menu);
    connect(ui->pushButton_back, &QPushButton::clicked,
            this, &GameWindow::save_score);
    connect(ui->tableWidget_field, &QTableWidget::cellEntered,
            this, &GameWindow::highlight);
    connect(ui->tableWidget_element, &QTableWidget::clicked,
            this, &GameWindow::switch_elements);
    connect(ui->tableWidget_field, &QTableWidget::cellClicked,
            this, &GameWindow::move);
}

GameWindow::~GameWindow() {
    delete ui;
}

void GameWindow::play(unsigned long long int best_score) {
    m_best_score = best_score;
    ui->label_score_best->setText(QString::fromStdString(std::to_string(m_best_score)));
    if (!m_info_was_shown) {
        emit CallAbout(); //show info window
        m_info_was_shown = true; //remember, that info was shown once
    }
    display_element();
    set_score();
}

void GameWindow::save_score() {
    if (m_tetris.score() > m_best_score) {
        emit SaveScore(m_tetris.score());
        m_best_score = m_tetris.score();
        ui->label_score_best->setText(QString::fromStdString(std::to_string(m_best_score)));
    }
}

void GameWindow::set_score() {
    ui->label_score->setText(QString::fromStdString(std::to_string(m_tetris.score())));
    if (m_tetris.score() > m_best_score) {
        ui->label_score_best->setText(QString::fromStdString(std::to_string(m_tetris.score())));
    }
}

void GameWindow::go_back_to_menu() {
    save_score();
    m_tetris.reset();
    ui->tableWidget_element->clear();
    ui->tableWidget_field->clear();
    set_score();
    emit GoBack();
    this->close();
}

void GameWindow::move(int x, int y) {
    int result = m_tetris.locate(x, y);
    display_field();
    display_element();
    set_score();
    if (result == GAMEOVER) {
        game_over();
    }
}

void GameWindow::switch_elements() {
    m_tetris.element_switch();
    display_element(); //update next element
    set_score(); // update current score
}

void GameWindow::display_element() {
    unsigned short element = m_tetris.element_next();
    ui->tableWidget_element->clear(); // clear field before modifying
    if (element == Tetris::T_DOT) {
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(2, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_SQUARE2) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1, 1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2, 1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1, 1)->setSelected(true);
        ui->tableWidget_element->item(1, 2)->setSelected(true);
        ui->tableWidget_element->item(2, 1)->setSelected(true);
        ui->tableWidget_element->item(2, 2)->setSelected(true);
    } else if (element == Tetris::T_SQUARE3) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(1, 1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1, 3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2, 1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2, 3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3, 1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3, 2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3, 3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1, 1)->setSelected(true);
        ui->tableWidget_element->item(1, 2)->setSelected(true);
        ui->tableWidget_element->item(1, 3)->setSelected(true);
        ui->tableWidget_element->item(2, 1)->setSelected(true);
        ui->tableWidget_element->item(2, 2)->setSelected(true);
        ui->tableWidget_element->item(2, 3)->setSelected(true);
        ui->tableWidget_element->item(3, 1)->setSelected(true);
        ui->tableWidget_element->item(3, 2)->setSelected(true);
        ui->tableWidget_element->item(3, 3)->setSelected(true);
    } else if (element == Tetris::T_HLINE2) {
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_HLINE3) {
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(2,3)->setSelected(true);
    } else if (element == Tetris::T_HLINE4) {
        ui->tableWidget_element->setItem(2, 0, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(2,0)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,0)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(2,3)->setSelected(true);
    } else if (element == Tetris::T_HLINE5) {
        ui->tableWidget_element->setItem(2, 0, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 4, new QTableWidgetItem());
        ui->tableWidget_element->item(2,0)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,4)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,0)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(2,3)->setSelected(true);
        ui->tableWidget_element->item(2,4)->setSelected(true);
    } else if (element == Tetris::T_VLINE2) {
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_VLINE3) {
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(3,2)->setSelected(true);
    } else if (element == Tetris::T_VLINE4) {
        ui->tableWidget_element->setItem(0, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(0,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(0,2)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(3,2)->setSelected(true);
    } else if (element == Tetris::T_VLINE5) {
        ui->tableWidget_element->setItem(0, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(4, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(0,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(4,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(0,2)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
        ui->tableWidget_element->item(3,2)->setSelected(true);
        ui->tableWidget_element->item(4,2)->setSelected(true);
    } else if (element == Tetris::T_BRANGLE2) {
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_BRANGLE3) {
        ui->tableWidget_element->setItem(1, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(1,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,3)->setSelected(true);
        ui->tableWidget_element->item(2,3)->setSelected(true);
        ui->tableWidget_element->item(3,1)->setSelected(true);
        ui->tableWidget_element->item(3,2)->setSelected(true);
        ui->tableWidget_element->item(3,3)->setSelected(true);
    } else if (element == Tetris::T_BLANGLE2) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_BLANGLE3) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(3,1)->setSelected(true);
        ui->tableWidget_element->item(3,2)->setSelected(true);
        ui->tableWidget_element->item(3,3)->setSelected(true);
    } else if (element == Tetris::T_TRANGLE2) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 2, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,2)->setSelected(true);
    } else if (element == Tetris::T_TRANGLE3) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 3, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(1,3)->setSelected(true);
        ui->tableWidget_element->item(2,3)->setSelected(true);
        ui->tableWidget_element->item(3,3)->setSelected(true);
    } else if (element == Tetris::T_TLANGLE2) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
    } else if (element == Tetris::T_TLANGLE3) {
        ui->tableWidget_element->setItem(1, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 2, new QTableWidgetItem());
        ui->tableWidget_element->setItem(1, 3, new QTableWidgetItem());
        ui->tableWidget_element->setItem(2, 1, new QTableWidgetItem());
        ui->tableWidget_element->setItem(3, 1, new QTableWidgetItem());
        ui->tableWidget_element->item(1,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,2)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,3)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(2,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(3,1)->setBackgroundColor(m_colors[0]);
        ui->tableWidget_element->item(1,1)->setSelected(true);
        ui->tableWidget_element->item(1,2)->setSelected(true);
        ui->tableWidget_element->item(1,3)->setSelected(true);
        ui->tableWidget_element->item(2,1)->setSelected(true);
        ui->tableWidget_element->item(3,1)->setSelected(true);
    }
}

void GameWindow::display_field() {
    ui->tableWidget_field->clear();
    std::vector<std::vector<int>> field = m_tetris.get_field();
    for (int i = 0; i < m_tetris.size(); ++i) {
        for (int j = 0; j < m_tetris.size(); ++j) {
            unsigned short cell = field[i][j];
            if (cell == 10) {
                display_cell(i, j, m_colors[0]);
            } else if (cell == 9) {
                display_cell(i, j, m_colors[1]);
            } else if (cell == 8) {
                display_cell(i, j, m_colors[2]);
            } else if (cell == 7) {
                display_cell(i, j, m_colors[3]);
            } else if (cell == 6) {
                display_cell(i, j, m_colors[4]);
            } else if (cell == 5) {
                display_cell(i, j, m_colors[5]);
            } else if (cell == 4) {
                display_cell(i, j, m_colors[6]);
            } else if (cell == 3) {
                display_cell(i, j, m_colors[7]);
            } else if (cell == 2) {
                display_cell(i, j, m_colors[8]);
            } else if (cell == 1) {
                display_cell(i, j, m_colors[9]);
            } else if (cell == 0) {
                display_cell(i, j, Qt::white);
            }
        }
    }
}

void GameWindow::display_cell(int i, int j, const QColor& color) {
    ui->tableWidget_field->setItem(i,j, new QTableWidgetItem());
    ui->tableWidget_field->item(i,j)->setBackgroundColor(color);
}

void GameWindow::highlight(int i, int j) {
    display_field();
    int size = m_tetris.size();
    unsigned short el_size = m_tetris.element_size();
    unsigned short element = m_tetris.element();
    if (element == Tetris::T_DOT || element == Tetris::T_SQUARE2 || element == Tetris::T_SQUARE3) {
        if (i+el_size > size || j+el_size > size) {
            return;
        }
        for (int k = 0; k < el_size; ++k) {
            for (int l = 0; l < el_size; ++l) {
                if (i+k < size && j+l < size) {
                    ui->tableWidget_field->setItem(i+k, j+l, new QTableWidgetItem());
                    ui->tableWidget_field->item(i+k,j+l)->setBackgroundColor(Qt::white);
                }
            }
        }
    } else if (element == Tetris::T_HLINE2 || element == Tetris::T_HLINE3 ||
               element == Tetris::T_HLINE4 || element == Tetris::T_HLINE5) {
        if (j+el_size > size) {
            return;
        }
        for (int k = 0; k < el_size; ++k) {
            if (j+k < size) {
                ui->tableWidget_field->setItem(i, j+k, new QTableWidgetItem());
                ui->tableWidget_field->item(i,j+k)->setBackgroundColor(Qt::white);
            }
        }
    } else if (element == Tetris::T_VLINE2 || element == Tetris::T_VLINE3 ||
               element == Tetris::T_VLINE4 || element == Tetris::T_VLINE5) {
        if (i+el_size > size) {
            return;
        }
        for (int k = 0; k < el_size; ++k) {
            if (i+k < size) {
                ui->tableWidget_field->setItem(i+k, j, new QTableWidgetItem());
                ui->tableWidget_field->item(i+k,j)->setBackgroundColor(Qt::white);
            }
        }
    } else if (element == Tetris::T_BRANGLE2 || element == Tetris::T_BRANGLE3 ||
               element == Tetris::T_BLANGLE2 || element == Tetris::T_BLANGLE3 ||
               element == Tetris::T_TRANGLE2 || element == Tetris::T_TRANGLE3 ||
               element == Tetris::T_TLANGLE2 || element == Tetris::T_TLANGLE3) {
        if (i+el_size > size || j+el_size > size) {
            return;
        }
        //check whether all position are empty:
        bool top = element == Tetris::T_TRANGLE2 || element == Tetris::T_TRANGLE3 || element == Tetris::T_TLANGLE2 || element == Tetris::T_TLANGLE3;
        for (int k = 0; k < el_size; ++k) {
            if ((top ? j+el_size-1 < size : i+el_size-1 < size && j+el_size-1 < size)) {
                ui->tableWidget_field->setItem((top ? i : i+el_size-1), j+k, new QTableWidgetItem());
                ui->tableWidget_field->item((top ? i : i+el_size-1),j+k)->setBackgroundColor(Qt::white);
            }
        }
        bool left = element == Tetris::T_TLANGLE2 || element == Tetris::T_TLANGLE3 || element == Tetris::T_BLANGLE2 || element == Tetris::T_BLANGLE3;
        for (int k = 0; k < el_size; ++k) {
            if ((left ? i+el_size-1 < size : i+el_size-1 < size && j+el_size-1 < size)) {
                ui->tableWidget_field->setItem(i+k, (left ? j : j+el_size-1), new QTableWidgetItem());
                ui->tableWidget_field->item(i+k, (left ? j : j+el_size-1))->setBackgroundColor(Qt::white);
            }
        }
    }
}

void GameWindow::game_over() {
    show_message('i', "GAME OVER!", "You score is: " + std::to_string(m_tetris.score()));
    save_score();
    m_tetris.reset();
    set_score();
    ui->tableWidget_field->clear();
    display_element();
}

/* show window (message box) with some info */
void GameWindow::show_message(char type, const std::__cxx11::string& title,
                              const std::__cxx11::string& body,
                              const std::__cxx11::string& detals) {
    QMessageBox window;
    if (type == 'q' || type == 'Q') {
        window.setIcon(QMessageBox::Question);
    } else if (type == 'i' || type == 'I') {
        window.setIcon(QMessageBox::Information);
    } else if (type == 'w' || type == 'W') {
        window.setIcon(QMessageBox::Warning);
    } else if (type == 'c' || type == 'C') {
        window.setIcon(QMessageBox::Critical);
    }
    window.setDefaultButton(QMessageBox::Ok); //create only one button
    window.setText("<b>"+QString::fromStdString(title)+"</b>"); //show title message
    if (body != "") {
        window.setInformativeText(QString::fromStdString(body)); //add informative text
    }
    if (detals != "") {
        window.setDetailedText(QString::fromStdString(detals)); //add detailed text
    }
    window.exec();
}
