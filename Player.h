/**
 * @file Player.h
 * @memberof authication system
 * @category account
 * @inherit User.h
 *
 * @brief Class of simple player.
 *
 * @details
 * Expand User class with score/email spaces and their methods.
 *
 * @author hell03end
 * @version 0.2.3
 * @date 04-Dec-16
 */

#ifndef PLAYER_H
#define PLAYER_H

#ifndef DEFPLR //default data for Player if they aren't set manually
#define DEFEML "some_email@example.com"
#endif //DEFPLR

#include "User.h"
#include <cstdio>

class Player : public User {
public:
    Player(const std::string& login = DEFLOG,
           const std::string& email = DEFEML,
           const std::string& password = DEFPAS); //(+err::err)
    virtual ~Player(void) {};

    virtual bool set_email(const std::string& email);
    virtual std::string get_email(void) const;

    void set_score(const unsigned long int score);
    unsigned long int get_score(void) const;

    virtual unsigned long int operator+(unsigned long int score);
    virtual unsigned long int operator-(unsigned long int score);
    virtual unsigned long int operator*(unsigned long int score);
    virtual bool operator==(const Player& player) const;
    virtual bool operator==(const User& user) const;
    virtual bool operator>(const Player& player) const;

    virtual std::string serialize(void) const; //transform object data into standard representation type
    virtual bool deserialize(const std::string& message); //create object from standard representation type

    operator User() const; //type cast operator
    static bool correct_email(const std::string& email); //check email format is correct
protected:
    std::string m_email; //nickname of player
    unsigned long int m_score; //max score of player
};

#endif //PLAYER_H
