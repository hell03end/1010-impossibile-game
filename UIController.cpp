#include "UIController.h"

static bool file_exists(const std::string& path) {
    std::ifstream fl(path);
    bool result = fl.is_open();
    fl.close();
    return result;
}

UIController::UIController(const std::string& path2db, QObject *parent)
    : QObject(parent), menu_ui(new MenuWindow()), info_ui(new InfoWindow()),
      game_ui(new GameWindow()), welcome_ui(new WelcomeWindow()), m_online(false) {
    /* Server */
    m_path2db = path2db;
    qDebug() << QString::fromStdString("Searching db in " + m_path2db + "...");
    if (!file_exists(m_path2db)) {
        /* creare new file */
        std::ofstream fl(m_path2db);
        fl.close();
        qDebug() << QString::fromStdString("DB isn't exists, creating new one...");
    } else {
        std::ifstream fl(m_path2db);
        std::string key;
        std::string value;
        qDebug() << QString::fromStdString("DB detected! Loading data...");
        m_mutex.lock();
        while (std::getline(fl, key) && std::getline(fl, value)) {
            if (key != "" && value != "") {
                m_db[key] = value;
                qDebug() << QString::fromStdString("User restored!");
            } else {
                qDebug() << QString::fromStdString("Can't read some data from stored data base!");
            }
        }
        m_mutex.unlock();
        fl.close();
    }

    /* Welcome window */
    connect(welcome_ui, &WelcomeWindow::CallAbout, this, &UIController::show_about);
    connect(welcome_ui, &WelcomeWindow::Geometry, this, &UIController::remember_geom);
    connect(welcome_ui, &WelcomeWindow::PlayOnline, this, &UIController::open_menu_from_welcome);
    connect(welcome_ui, &WelcomeWindow::SignUp, this, &UIController::user_add);
    connect(welcome_ui, &WelcomeWindow::SignIn, this, &UIController::user_log_in);

    /* Menu window */
    connect(menu_ui, &MenuWindow::CallAbout, this, &UIController::show_about);
    connect(menu_ui, &MenuWindow::Geometry, this, &UIController::remember_geom);
    connect(menu_ui, &MenuWindow::Play, this, &UIController::open_game_w);
    connect(menu_ui, &MenuWindow::RestScore, this, &UIController::reset_score);
    connect(menu_ui, &MenuWindow::DeleteUser, this, &UIController::user_delete);
    connect(menu_ui, &MenuWindow::LogOut, this, &UIController::open_welcome_w);
    connect(menu_ui, &MenuWindow::ShowPlayers, this, &UIController::players_show);

    /* Game window */
    connect(game_ui, &GameWindow::GoBack, this, &UIController::open_menu_from_game);
    connect(game_ui, &GameWindow::SaveScore, this, &UIController::set_score);
    connect(game_ui, &GameWindow::CallAbout, this, &UIController::show_about);

    welcome_ui->show(); //show welcome window
}

UIController::~UIController() {
    std::ofstream fl(m_path2db);
    qDebug() << QString::fromStdString("Saving data base to file...");
    m_mutex.lock();
    for (Dictionary::iterator iter = m_db.begin(); iter != m_db.end(); ++iter) {
        fl << iter->first << std::endl;
        fl << iter->second << std::endl;
        qDebug() << QString::fromStdString("User successfully saved.");
    }
    m_mutex.unlock();
    fl.close();
    delete welcome_ui;
    delete menu_ui;
    delete game_ui;
    delete info_ui;
}

void UIController::show_about() {
    info_ui->show();
}

void UIController::remember_geom(const QRect& size) {
    m_curr_geom = size;
}

void UIController::open_menu_from_welcome(bool key) {
    if (key) {
        m_online = true;
        qDebug() << QString::fromStdString("Online mode started...");
        menu_ui->set_online_settings(m_player.get_login(), m_player.get_score());
    } else {
        m_online = false;
        qDebug() << QString::fromStdString("Offline mode started...");
        menu_ui->set_offline_settings();
    }
    menu_ui->set_geometry(m_curr_geom);
    menu_ui->setEnabled(true);
    menu_ui->show();
}

void UIController::user_add(const std::__cxx11::string& login,
                            const std::__cxx11::string& password,
                            const std::__cxx11::string& email) {
    if (!m_player.set_login(login) || !m_player.set_email(email) || !m_player.set_password(password)) {
        welcome_ui->signed_up(WRGDAT1);
        qDebug() << QString::fromStdString("User can't be created due to bad input data!");
        return;
    }
    User user(login, password);
    m_mutex.lock();
    Dictionary::iterator iter = m_db.find(user.serialize());
    if (iter != m_db.end()) {
        m_mutex.unlock();
        qDebug() << QString::fromStdString("User can't be created because user with such credentials already exists!");
        welcome_ui->signed_up(WRGDAT);
    } else {
        m_db[user.serialize()] = m_player.serialize();
        reset_score();
        m_mutex.unlock();
        qDebug() << QString::fromStdString("New user successfully created!");
        welcome_ui->signed_up(SUCCESS);
    }
}

void UIController::user_log_in(const std::__cxx11::string& login,
                                     const std::__cxx11::string& password) {
    if (!m_player.set_login(login) || !m_player.set_password(password)) {
        qDebug() << QString::fromStdString("User can't be found due to bad input data!");
        welcome_ui->signed_in(WRGDAT1);
        return;
    }
    User user(login, password);
    m_mutex.lock();
    Dictionary::iterator iter = m_db.find(user.serialize());
    m_mutex.unlock();
    if (iter != m_db.end()) {
        qDebug() << QString::fromStdString("User found! Checking data...");
        welcome_ui->signed_in((m_player.deserialize(iter->second) ? SUCCESS : ACCDEN));
    } else {
        qDebug() << QString::fromStdString("Can't find such user!");
        welcome_ui->signed_in(WRGDAT1);
    }
}

void UIController::open_game_w() {
    game_ui->setEnabled(true);
    game_ui->show();
    qDebug() << QString::fromStdString("New game started!");
    game_ui->play(m_player.get_score());
}

void UIController::reset_score() {
    if (m_online) {
        m_player.set_score(0);
        User user = static_cast<User>(m_player);
        m_db[user.serialize()] = m_player.serialize();
        qDebug() << QString::fromStdString("Score of " + m_player.get_login() + " restored!");
    }
}

void UIController::user_delete() {
//    server.remove();
    if (m_online) {
        User user(m_player.get_login(), m_player.get_password());
        Dictionary::iterator iter = m_db.find(user.serialize());
        if (iter != m_db.end()) {
            m_db.erase(iter);
            qDebug() << QString::fromStdString("User successfully was deleted.");
        }
    }
    open_welcome_w();
}

void UIController::open_welcome_w() {
    m_player.set_score(0);
    welcome_ui->set_geometry(m_curr_geom);
    welcome_ui->setEnabled(true);
    welcome_ui->show();
}

void UIController::players_show() {
    /* send players to output */
    menu_ui->clear_palyers();
    QMutexLocker locker(&m_mutex);
    menu_ui->show_player("Login", "Email", 0);
    for (Dictionary::iterator iter = m_db.begin(); iter != m_db.end(); ++iter) {
        Player player;
        player.deserialize(iter->second);
        menu_ui->show_player(player.get_login(), player.get_email(), player.get_score());
    }
}

void UIController::set_score(unsigned long long score) {
    if (m_player.get_score() < score) {
        m_player.set_score(score);
        if (m_online) {
            User user = static_cast<User>(m_player);
            m_db[user.serialize()] = m_player.serialize();
            qDebug() << QString::fromStdString("New score ("+std::to_string(m_player.get_score())+") of "+m_player.get_login()+" saved!");
        }
    }
}

void UIController::open_menu_from_game() {
    open_menu_from_welcome(m_online);
}
