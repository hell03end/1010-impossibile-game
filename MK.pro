#-------------------------------------------------
#
# Project created by hell03end 2016-10-10T23:43:48
# Updated by hell03end 2016-15-10 all files included + folders added
# Updated by hell03end 2016-18-12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MK
TEMPLATE = app


SOURCES += main.cpp\
    WelcomeWindow.cpp \
    GameWindow.cpp \
    User.cpp \
    Player.cpp \
#    Server.cpp \
    MenuWindow.cpp \
    UIController.cpp \
    InfoWindow.cpp \
    sha512.cpp

HEADERS  += \
    WelcomeWindow.h \
    GameWindow.h \
    BaseObject.inl \
    User.h \ #class for user
    Player.h \ #class for player
    ErrMessages.h \  #handling with errors
    Tetris.inl \
#    Server.h \
    MenuWindow.h \
    UIController.h \
    InfoWindow.h \
    sha512.h
#    UnitTests/CommonTest.inl \
#    UnitTests/PlayerTest.inl \
#    UnitTests/UserTest.inl \
#    UnitTests/UnitTests.inl

FORMS    += \
    WelcomeWindow.ui \
    GameWindow.ui \
    InfoWindow.ui \
    MenuWindow.ui

CONFIG += c++17 #why not?:)

RESOURCES += \
    rsc.qrc
