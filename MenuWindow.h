#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ErrMessages.h"
#include "Player.h"
#include <QMainWindow> //should be QWidget
#include <QMessageBox>

namespace Ui {
    class MainWindow;
}

class MenuWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MenuWindow(QWidget *parent = NULL);
    ~MenuWindow(void);

    void set_offline_settings(void);
    void set_online_settings(const std::string& login, unsigned long long int score);
    void show_player(const std::string& login, const std::string& email,
                     unsigned long long int score);
    void clear_palyers(void);
    void update_score(unsigned long long int score);
    void set_geometry(const QRect& size);

private:
    Ui::MainWindow* ui; //GUI for main window

    inline void rest_score(void);
    inline void call_about(void);
    inline void log_out(void);
    inline void delete_user(void);

    inline void play(void);
    inline void go_to_main_window(void);
    inline void go_to_scores(void);
    inline void go_to_settings(void);
    void show_message(char type, const std::string& title,
                      const std::string& body = "", const std::string& detals = "");

signals:
    void Play(void);
    void RestScore(void);
    void DeleteUser(void);
    void LogOut(void);
    void ShowPlayers(void); // <-- ??? Is it necessary?
    void CallAbout(void); //show info window
    void Geometry(const QRect&); //return geometry of window
};

#endif // MAINWINDOW_H
