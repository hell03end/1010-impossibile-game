#include "UIController.h"
//#include "UnitTests/UnitTests.inl"
#include <QApplication>
#include <QObject>
//#include <QThread>

int main(int argc, char *argv[]) {
    QApplication application(argc, argv); //Start point of application
    UIController client_controller; //create controller
//    QThread unit_tests_thread;
//    UnitTests tests;

//    QObject::connect(&unit_tests_thread, &QThread::started, &tests, &UnitTests::run);
//    QObject::connect(&tests, &UnitTests::Finished, &unit_tests_thread, &QThread::exit);

//    tests.moveToThread(&unit_tests_thread);
//    unit_tests_thread.start();

    return application.exec(); //return value from QApplication
}
