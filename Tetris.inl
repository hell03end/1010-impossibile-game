#ifndef TETRIS
#define TETRIS

#include "ErrMessages.h"
#include <vector>
#include <random>
#include <ctime>

class Tetris {
public:
    enum object_types {
        T_EMPTY, //0 empty cell
        T_DOT, T_SQUARE2, T_SQUARE3, // 1-3 squares
        T_HLINE2, T_HLINE3, T_HLINE4, T_HLINE5, // 4-7 horizontal lines
        T_VLINE2, T_VLINE3, T_VLINE4, T_VLINE5, // 8-11 vertical lines
        T_TRANGLE2, T_TLANGLE2, T_BRANGLE2, T_BLANGLE2, // 12-15 2-angles
        T_TRANGLE3, T_TLANGLE3, T_BRANGLE3, T_BLANGLE3 // 16-19 3-angles
    }; // types of game objects

    Tetris(unsigned int size = 10) : m_field(new std::vector< std::vector<int> >(size, std::vector<int>(size))) {
        m_score = 0;
        clear_field();
    }
    virtual ~Tetris(void) { delete m_field; }

    void element_switch(void) {
        Element<unsigned short> tmp = m_element;
        m_element = m_element_next;
        m_element_next = tmp;
        m_score = (m_score <= m_element.points()) ? 0 : m_score - m_element.points();
    }
    inline void reset(void) { m_score = 0; clear_field(); m_element.reset(); m_element_next.reset(); } // clear game session
    inline unsigned long long int size(void) const { return m_field->size(); } // return size of field
    inline unsigned long long int score(void) const { return m_score; } // return current score
    inline unsigned short element(void) const { return m_element.type(); } // return type of current element
    inline unsigned short element_next(void) const { return m_element_next.type(); } // return type of next element
    inline unsigned short element_size(void) const { return m_element.size(); }
    inline unsigned short element_size_next(void) const { return m_element_next.size(); }
    inline const std::vector<std::vector<int>>& get_field(void) { return *m_field; }
    int locate(unsigned int x, unsigned int y) {
        if (x > m_field->size() || y > m_field->size()) {
            return WRNGMOVE;
        }
        if (m_element.type() == T_DOT || m_element.type() == T_SQUARE2 || m_element.type() == T_SQUARE3) {
            if (!add_square(x, y)) {
                return WRNGMOVE;
            }
        } else if (m_element.type() == T_HLINE2 || m_element.type() == T_HLINE3 || m_element.type() == T_HLINE4 ||
                   m_element.type() == T_HLINE5 || m_element.type() == T_VLINE2 || m_element.type() == T_VLINE3 ||
                   m_element.type() == T_VLINE4 || m_element.type() == T_VLINE5) {
            if (!add_line(x, y)) {
                return WRNGMOVE;
            }
        } else if (m_element.type() == T_TRANGLE2 || m_element.type() == T_TRANGLE3 || m_element.type() == T_TLANGLE2 ||
                   m_element.type() == T_TLANGLE3 || m_element.type() == T_BRANGLE2 || m_element.type() == T_BRANGLE3 ||
                   m_element.type() == T_BLANGLE2 || m_element.type() == T_BLANGLE3) {
            if (!add_angle(x, y)) {
                return WRNGMOVE;
            }
        }
        m_score += m_element.points();
        m_element = m_element_next;
        m_element_next.reset();
        m_score += check_combinations();
        reload_timers(-1);
        if (!check_free_space() || check_exploded()) {
            return GAMEOVER;
        }
        return SUCCESS;
    } // main mathod to interruct with game
private:
    enum object_points {
        P_NONE = 0, // for empty element
        P_DOT = 1, P_SQUARE2 = 4, P_SQUARE3 = 9, // 1-3 squares
        P_LINE2 = 2, P_LINE3 = 3, P_LINE4 = 4, P_LINE5 = 5, // 4-7 lines
        P_ANGLE2 = 3, P_ANGLE3 = 5, // 8-9 angles
        P_SIMPLE_COMBINATION = 20, P_CLEAR_FIELD = 100, // 10-11 combinations (rows/columns)
    }; // points per object
    enum object_timer { V_EXPLODED = 0, V_UNLOADED = -1, V_LOADED = 11 };
    template <class T>
    class Element {
    public:
        Element(void) { srand(static_cast<unsigned int>(time(NULL))); reload(); reset(); }
        virtual ~Element() { /* Nothing */ }
        void set_type(T type = T_EMPTY) {
            m_type = (type) ? type : T_EMPTY;
            if (m_type == T_EMPTY) {
                m_size = 0;
                m_points = P_NONE;
            } else if (m_type == T_DOT) {
                m_size = 1;
                m_points = P_DOT;
            } else if (m_type == T_SQUARE2 || m_type == T_HLINE2 || m_type == T_VLINE2 || m_type == T_TRANGLE2 ||
                       m_type == T_TLANGLE2 || m_type == T_BRANGLE2 || m_type == T_BLANGLE2) {
                m_size = 2;
                if (m_type == T_SQUARE2) {
                    m_points = P_SQUARE2;
                } else if (m_type == T_HLINE2 || m_type == T_VLINE2) {
                    m_points = P_LINE2;
                } else if (m_type == T_TRANGLE2 || m_type == T_TLANGLE2 || m_type == T_BRANGLE2 || m_type == T_BLANGLE2) {
                    m_points = P_ANGLE2;
                }
            } else if (m_type == T_SQUARE3 || m_type == T_HLINE3 || m_type == T_VLINE3 || m_type == T_TRANGLE3 ||
                       m_type == T_TLANGLE3 || m_type == T_BRANGLE3 || m_type == T_BLANGLE3) {
                m_size = 3;
                if (m_type == T_SQUARE3) {
                    m_points = P_SQUARE3;
                } else if (m_type == T_HLINE3 || m_type == T_VLINE3) {
                    m_points = P_LINE3;
                } else if (m_type == T_TRANGLE3 || m_type == T_TLANGLE3 || m_type == T_BRANGLE3 || m_type == T_BLANGLE3) {
                    m_points = P_ANGLE3;
                }
            } else if (m_type == T_HLINE4 || m_type == T_VLINE4) {
                m_size = 4;
                m_points = P_LINE4;
            } else if (m_type == T_HLINE5 || m_type == T_VLINE5) {
                m_size = 5;
                m_points = P_LINE5;
            }
        }
        inline T type(void) const { return m_type; }
        inline void set_timer(void) { m_timer = V_LOADED; }
        inline short timer(void) const { return m_timer; }
        inline unsigned short size(void) const { return m_size; }
        inline long points(void) const { return m_points; }
        inline void reset(void) { set_type(random()); reload(); } // set new type to element
        inline void reload(object_timer timer = V_LOADED) { m_timer = timer; } // load timer again
        inline void clear(void) { m_type = T_EMPTY; m_timer = V_UNLOADED; } // remove all data from element
        Element<T>& operator =(const Element<T>& element) {
            if (this != &element) {
                m_type = element.m_type;
                m_points = element.m_points;
                m_size = element.m_size;
                m_timer = element.m_timer;
            }
            return *this;
        }
    private:
        T m_type; // geometric type of figure
        long m_points; // points for setting object
        unsigned short m_size; // size of figure
        short m_timer; // timer for element to destroy

        T random() const {
            int n = rand()%19000;
            if (n < 800) {
                return T_DOT;
            } else if (n < 1800) {
                return T_SQUARE2;
            } else if (n < 2400) {
                return T_SQUARE3;
            } else if (n < 3710) {
                return T_HLINE2;
            } else if (n < 5020) {
                return T_HLINE3;
            } else if (n < 6330) {
                return T_HLINE4;
            } else if (n < 7080) {
                return T_HLINE5;
            } else if (n < 8390) {
                return T_VLINE2;
            } else if (n < 9700) {
                return T_VLINE3;
            } else if (n < 11010) {
                return T_VLINE4;
            } else if (n < 11760) {
                return T_VLINE5;
            } else if (n < 13070) {
                return T_BLANGLE2;
            } else if (n < 13570) {
                return T_BLANGLE3;
            } else if (n < 14880) {
                return T_BRANGLE2;
            } else if (n < 15380) {
                return T_BRANGLE3;
            } else if (n < 16690) {
                return T_TRANGLE2;
            } else if (n < 17190) {
                return T_TRANGLE3;
            } else if (n < 18500) {
                return T_TLANGLE2;
            } else if (n < 19000) {
                return T_TLANGLE3;
            } else {
                return 1 + rand()%20;
            }
        }
    };

    std::vector< std::vector<int> >* m_field; // game field
    Element<unsigned short> m_element; // current element
    Element<unsigned short> m_element_next; //next element
    unsigned long long int m_score; // current score

    long long int check_combinations(void) {
        std::vector<int> rows;
        std::vector<int> columns;
        for (int i = 0; i < m_field->size(); ++i) {
            int j = 0;
            for ( ; j < m_field->size() && (*m_field)[i][j] > V_EXPLODED; ++j)
                ;
            if (j == m_field->size()) {
                rows.push_back(i);
            }
            j = 0;
            for ( ; j < m_field->size() && (*m_field)[j][i] > V_EXPLODED; ++j)
                ;
            if (j == m_field->size()) {
                columns.push_back(i);
            }
        }
        for (auto i : rows) {
            for (int j = 0; j < m_field->size(); ++j) {
                (*m_field)[i][j] = V_UNLOADED;
            }
        }
        for (auto i : columns) {
            for (int j = 0; j < m_field->size(); ++j) {
                (*m_field)[j][i] = V_UNLOADED;
            }
        }
        if (rows.size() > 2 || columns.size() > 2 || (columns.size() > 1 && rows.size() > 1)) {
            clear_field();
        }
        return (rows.size() > 1 || columns.size() > 1 || (columns.size() > 1 && rows.size() > 1)) ? P_CLEAR_FIELD : P_SIMPLE_COMBINATION*(rows.size() + columns.size());
    } // find all combinations on field return points won
    bool check_free_space(void) const {
        bool space = false;
        if (m_element.type() == T_DOT) {
            for (int i = 0; i < m_field->size(); ++i) {
                for (int j = 0; j < m_field->size(); ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_SQUARE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+1] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_SQUARE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i + 1][j + 1] == V_UNLOADED &&
                        (*m_field)[i + 2][j + 2] == V_UNLOADED && (*m_field)[i][j + 1] == V_UNLOADED &&
                        (*m_field)[i][j + 2] == V_UNLOADED && (*m_field)[i + 1][j + 2] == V_UNLOADED &&
                        (*m_field)[i + 1][j] == V_UNLOADED && (*m_field)[i + 2][j] == V_UNLOADED &&
                        (*m_field)[i + 2][j + 1] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_HLINE2) {
            for (int i = 0; i < m_field->size() && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_HLINE3) {
            for (int i = 0; i < m_field->size() && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+2] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_HLINE4) {
            for (int i = 0; i < m_field->size() && !space; ++i) {
                for (int j = 0; j < m_field->size()-3 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+2] == V_UNLOADED && (*m_field)[i][j+3] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_HLINE5) {
            for (int i = 0; i < m_field->size() && !space; ++i) {
                for (int j = 0; j < m_field->size()-4 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+2] == V_UNLOADED && (*m_field)[i][j+3] == V_UNLOADED &&
                        (*m_field)[i][j+4] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_VLINE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size() && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_VLINE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size() && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i+2][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_VLINE4) {
            for (int i = 0; i < m_field->size()-3 && !space; ++i) {
                for (int j = 0; j < m_field->size() && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i+2][j] == V_UNLOADED && (*m_field)[i+3][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_VLINE5) {
            for (int i = 0; i < m_field->size()-4 && !space; ++i) {
                for (int j = 0; j < m_field->size() && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i+2][j] == V_UNLOADED && (*m_field)[i+3][j] == V_UNLOADED &&
                        (*m_field)[i+4][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_TRANGLE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+1] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_TLANGLE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i][j+1] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_BRANGLE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i+1][j+1] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i][j+1] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_BLANGLE2) {
            for (int i = 0; i < m_field->size()-1 && !space; ++i) {
                for (int j = 0; j < m_field->size()-1 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i+1][j+1] == V_UNLOADED &&
                        (*m_field)[i+1][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_TRANGLE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+2] == V_UNLOADED && (*m_field)[i+1][j+2] == V_UNLOADED &&
                        (*m_field)[i+2][j+2] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_TLANGLE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i][j] == V_UNLOADED && (*m_field)[i][j+1] == V_UNLOADED &&
                        (*m_field)[i][j+2] == V_UNLOADED && (*m_field)[i+1][j] == V_UNLOADED &&
                        (*m_field)[i+2][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_BRANGLE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i+2][j] == V_UNLOADED && (*m_field)[i+2][j+1] == V_UNLOADED &&
                        (*m_field)[i+2][j+2] == V_UNLOADED && (*m_field)[i][j+2] == V_UNLOADED &&
                        (*m_field)[i+1][j+2] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        } else if (m_element.type() == T_BLANGLE3) {
            for (int i = 0; i < m_field->size()-2 && !space; ++i) {
                for (int j = 0; j < m_field->size()-2 && !space; ++j) {
                    if ((*m_field)[i+2][j] == V_UNLOADED && (*m_field)[i+2][j+1] == V_UNLOADED &&
                        (*m_field)[i+2][j+2] == V_UNLOADED && (*m_field)[i][j] == V_UNLOADED &&
                        (*m_field)[i+1][j] == V_UNLOADED) {
                        space = true;
                    }
                }
            }
        }
        return space;
    } // check whether is free space for next element
    bool check_exploded(void) const {
        for (auto line : *m_field) {
            for (auto i : line) {
                if (i == V_EXPLODED) {
                    return true;
                }
            }
        }
        return false;
    } // check exploded elements
    void clear_field(void) {
        for (int i = 0; i < m_field->size(); ++i) {
            for (int j = 0; j < m_field->size(); ++j) {
                (*m_field)[i][j] = V_UNLOADED;
            }
        }
    } // unload any cell on field
    void reload_timers(int n) {
        for (int i = 0; i < m_field->size(); ++i) {
            for (int j = 0; j < m_field->size(); ++j) {
                if ((*m_field)[i][j] > V_EXPLODED) {
                    (*m_field)[i][j] += n;
                }
            }
        }
    } // decrease any loaded position of field by 1

    bool add_square (unsigned int x, unsigned int y) {
        if (x + m_element.size() > m_field->size() || y + m_element.size() > m_field->size()) {
            return false;
        }
        /* check whether all position are empty: */
        for (int i = x; i < x + m_element.size(); ++i) {
            for (int j = y; j < y + m_element.size(); ++j) {
                if ((*m_field)[i][j] != V_UNLOADED) {
                    return false;
                }
            }
        }
        for (int i = x; i < x + m_element.size(); ++i) {
            for (int j = y; j < y + m_element.size(); ++j) {
                (*m_field)[i][j] = m_element.timer();
            }
        }
        return true;
    } // add squere on field if possible
    bool add_line (unsigned int x, unsigned int y) {
        bool key = m_element.type() == T_HLINE2 || m_element.type() == T_HLINE3 || m_element.type() == T_HLINE4 || m_element.type() == T_HLINE5;
        if (((key) ? y : x) + m_element.size() > m_field->size()) {
            return false;
        }
        for (int i = 0; i < m_element.size(); ++i) {
            if ((*m_field)[((key) ? x : x + i)][((key) ? y + i : y)] != V_UNLOADED) {
                return false;
            }
        }
        for (int i = 0; i < m_element.size(); ++i) {
            (*m_field)[((key) ? x : x + i)][((key) ? y + i : y)] = m_element.timer();
        }
        return true;
    } // add line on field if possible
    bool add_angle (unsigned int x, unsigned int y) {
        if (x + m_element.size() > m_field->size() || y + m_element.size() > m_field->size()) {
            return false;
        }
        //check whether all position are empty:
        bool top = m_element.type() == T_TRANGLE2 || m_element.type() == T_TRANGLE3 || m_element.type() == T_TLANGLE2 || m_element.type() == T_TLANGLE3;
        for (int i = y; i < y + m_element.size(); ++i) {
            if ((*m_field)[((top) ? x : x + m_element.size() - 1)][i] != V_UNLOADED) {
                return false;
            }
        }
        bool left = m_element.type() == T_TLANGLE2 || m_element.type() == T_TLANGLE3 || m_element.type() == T_BLANGLE2 || m_element.type() == T_BLANGLE3;
        for (int i = x; i < x + m_element.size(); ++i) {
            if ((*m_field)[i][((left) ? y : y + m_element.size() - 1)] != V_UNLOADED) {
                return false;
            }
        }
        for (int i = y; i < y + m_element.size(); ++i) {
            (*m_field)[((top) ? x : x + m_element.size() - 1)][i] = V_LOADED;
        }
        for (int i = x; i < x + m_element.size(); ++i) {
            (*m_field)[i][((left) ? y : y + m_element.size() - 1)] = V_LOADED;
        }
        return true;
    } // add angle on field if possible
};

#endif //TETRIS
