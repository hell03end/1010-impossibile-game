#ifndef SERVER_H
#define SERVER_H

#include "ErrMessages.h"
#include <map>
#include <QMutex>
#include <fstream>
#include <QObject>
#include <unistd.h>

template <class Key = std::string, class T = std::string>
class Server : public QObject {
    Q_OBJECT
public:
    explicit Server(const std::string& db_path = "db", QObject *parent = NULL);
    virtual ~Server(); // to save current resorses and close server

    void sign_in(const T& key);
    void sign_up(const Key&, const T& value);
    void remove(const Key&);
    inline void get_players(int n = -1);
    inline void update(const Key&, const T& value);
    inline unsigned int size(void) const;

signals:
    void SignedUp(bool);
    void SignedIn(const T& value);
    void GetUser(const T& value);

private:
    typedef std::map<Key, T> Dictionary;

    Dictionary m_db; // map with information about players
    std::string m_db_path; // name of file with information about players
    QMutex m_mutex; // to prevent simultaneous access to DB

    Key find_in_db(const T& value) const;
    static bool file_exists(const std::string& path);
};

#endif // SERVER_H
