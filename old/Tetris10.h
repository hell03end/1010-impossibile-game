#ifndef TETRIS1010
#define TETRIS1010

#include <cstdlib> // for memory control
#include <ctime>

#ifndef min
#define min(a,b) ((a > b) ? (b) : (a))
#endif //min

#ifndef ZERO
#define ZERO(a, b) (field.space[i + (a)][j + (b)] == 0)
#endif //ZERO

#ifndef SIZE
#define SIZE(a) (field.size() - (a))
#endif //SIZE

#ifndef GAME_OBJECTS
#define GAME_OBJECTS
//types of game objects:
enum objects {
    DOT = 1, SQUARE2, SQUARE3, // 1-3 squares
    HLINE2, HLINE3, HLINE4, HLINE5, // 4-7 horizontal lines
    VLINE2, VLINE3, VLINE4, VLINE5, // 8-11 vertical lines
    TRANGLE2, TLANGLE2, BRANGLE2, BLANGLE2, // 12-15 2-angles
    TRANGLE3, TLANGLE3, BRANGLE3, BLANGLE3 // 16-19 3-angles
    /* <-- New types can be placed here! --> */
};
#endif //GAME_OBJECTS

#ifndef GAME_OBJECT_POINTS
#define GAME_OBJECT_POINTS
//points per object:
enum points {
    DOT_POINTS = 1, SQUARE2_POINTS = 4, SQUARE3_POINTS = 9, // 1-3 squares
    LINE2_POINTS = 2, LINE3_POINTS = 3, LINE4_POINTS = 4, LINE5_POINTS = 5, // 4-7 lines
    ANGLE2_POINTS = 3, ANGLE3_POINTS = 5, // 8-9 angles
    SIMPLE_COMBINATION = 15, CLEAR_FIELD = 500, // 10-11 combinations (rows/columns)
    BONUS = 100, BONUS_CROSS = 100 // 12-13 bonuses
    /* <-- New types can be placed here! --> */
};
#endif //GAME_OBJECT_POINTS

#ifndef GAME_OBJECT_VALUES
#define GAME_OBJECT_VALUES
//values for game objects [element_value_(optional)level]:
enum objects_value {
    first_level = 1, // simple element
    second_level, // harder element
    third_level, // --||--
    /* <-- New types can be placed here! --> */
};
#endif //GAME_OBJECT_VALUES

#ifndef GAME_BONUSES
#define GAME_BONUSES
enum bonus_value {
    bonus_1 = 1
    /* <-- New types can be placed here! --> */
};
#endif //GAME_BONUSES

#ifndef GAME_FIELD_ROW
#define GAME_FIELD_ROW
typedef int* Row; // one row in a game field
#endif //GAME_FIELD_ROW

#ifndef GAME_FIELD_SPACE
#define GAME_FIELD_SPACE
typedef int** Space; // game field
#endif //GAME_FIELD_SPACE

#ifndef GAME_POSITION
#define GAME_POSITION
typedef struct { int x; int y; } Position; // to locate element in the field
#endif //GAME_POSITION

static int random (int from, int to = 1000) {
    return (rand() % (to - from)) + from;
} // generate random value

template <class T>
class Field {
public:
    Field(unsigned int size = 10, T level = 0) {
        m_size = size;
        m_level = level; // initial depth
        m_space = (Space) calloc(m_size, sizeof(T)); // memory for the field
        for (int i = 0; i < m_size; ++i) {
            m_space[i] = (Row) calloc(m_size, sizeof(T));
            for (int j = 0; j < m_size; ++j) {
                m_space[i][j] = m_level; //fill
            }
        }
    };
    ~Field() {
        for (int i = 0; i < m_size; ++i) {
            free(m_space[i]);
        }
        free(m_space);
        m_space = NULL;
    };
    unsigned int size(void) const { return m_size; };
    unsigned int level(void) const { return m_level; };
    void set_level(unsigned int n) { m_level = n; };
    virtual T* operator[](unsigned int i) {
        if (i >= m_size) {
            i = 0;
        }
        return m_space[i];
    };
private:
    Space m_space; // game_field space
    unsigned int m_size; // should be 6-(18?)
    unsigned int m_level; // for depth level
};

class Element {
public:
    Element(unsigned short type = 0, unsigned short value = 0, unsigned short bonus = 0) {
        m_bonus = (bonus >= 0 && bonus <= 1) ? bonus : 0;
        m_value = (value) ? value : 0;
        m_type = (type) ? type : 0;
    };
    virtual ~Element() {};
    inline void set_type(void) { m_type = random(1, 20); };
    inline unsigned short get_type(void) const { return m_type; };
    void set_value(void) {
        for (m_value = 0; m_value == 0; m_value = 0) {
            m_value = random(0, 10);
            if (m_value >= 0 && m_value < 5) {
                m_value = objects_value::first_level;
            } else if (m_value >= 5 && m_value < 8) {
                m_value = objects_value::second_level;
            } else if (m_value == 8 || m_value == 9) {
                m_value = objects_value::third_level;
            }
        }
    };
    inline unsigned short get_value(void) const { return m_value; };
    inline void set_bonus(void) { m_bonus = (random(0, 10) == 7) ? bonus_value::bonus_1 : 0; };
    inline unsigned short get_bonus(void) const { return m_bonus; };
    inline void clear(void) { m_type = 0; m_value = 0; m_bonus = 0; }; //remove all data from element
private:
    unsigned short m_type; // geometric type of figure
    unsigned short m_value; // value is for level of element
    unsigned short m_bonus;
};

class ElementsPanel {
public:
    ElementsPanel(void) : m_amount(0) { fill(); };
    virtual ~ElementsPanel(void) {};
    // fill elements panel with 3 random elements:
    void fill(unsigned short amount = 3) {
        srand(time(NULL)); // set seed for generator (bad variant)
        m_amount = (amount > 0 && amount < 4) ? amount : 3;
        for (int i = 0; i < m_amount; ++i) {
            m_objects[i].set_type(); // set random object type
            m_objects[i].set_value(); // set random object value
            m_objects[i].set_bonus(); // set random object bonus
        }
    };
    inline unsigned short size(void) const { return m_amount; };
    inline Element get_element(unsigned short i = 1) {
        if (i < 1 || i  > 3) {
            i = 1;
        }
        Element result = m_objects[i-1];
        m_objects[i-1].clear();
        m_amount--;
        if (m_amount == 0) {
            fill(); // fill elements panel again if there are no elements
        }
        return result;
    }; //get element and remove it from panel
    virtual Element operator[](unsigned short i = 0) const {
        if (i  > 2) {
            i = 0;
        }
        return m_objects[i];
    };
private:
    Element m_objects[3];
    unsigned short int m_amount;
};

class Tetris10 {
public:
    Tetris10(void) : m_score(0) { /* Nothing */ };
    virtual ~Tetris10(void) { /* Nothing */ };
    unsigned long start(void) {
        while (true) {
            int number = 0;

            m_score += check_combinations();
            if (check_space()) {
//                field_output(game_field);
//                printf("Your score is:\t%d\n", score);
//                printf("GAME OVER!");
                return m_score;
            }

            //output:
//            field_output(game_field);
//            elpnl_output(game_panel); // displays elements panel (temp?)

            number = choose(game_panel, &element);
            locate(game_field, &position);
            if (switcher(game_field, position, &element, &score)) {
                m_elements_panel.amount--;
                //delete used element from game panel
                if (number == 1)
                    m_elements_panel.object1 = element;
                else if (number == 2)
                    m_elements_panel.object2 = element;
                else if (number == 3)
                    m_elements_panel.object3 = element;
            }
        }
    }; //game cycle
private:
    Element curr_element; // chosen element
    Position position; // chosen position
    Field<int> m_field;
    ElementsPanel m_elements_panel;
    unsigned long int m_score;

    unsigned int check_combinations (void) { return 0; }; // find absolutely ful lines/rows, destroy them and return score
    int check_space () { return 0; }; // check whether are free space for current elements from elements panel in current field, return 1 if not
    static char check_element_with_value (Element); //@undone

    // to add main figures:
    // type name (int x_placement, int y_placement, int size/*for ComplexFigures*/, int value);
    int add_dot (int x, int y, int value); // add simple one-dot figure
    int add_hline (int x, int y, int size_of_figure, int value); // add horizontal line (2/3/4/5)
    int add_vline (int x, int y, int size_of_figure, int value); // add vertical line (2/3/4/5)
    int add_square (int x, int y, int size_of_figure, int value); // add square (2/3)
    int add_trangle (int x, int y, int size_of_figure, int value); // add top-right-angle (2/3)
    int add_tlangle (int x, int y, int size_of_figure, int value); // add bottom-left-angle (2/3)
    int add_brangle (int x, int y, int size_of_figure, int value); // add top-right-angle (2/3)
    int add_blangle (int x, int y, int size_of_figure, int value); // add bottom-left-angle (2/3)

    static int choose (ElementsPanel panel, Element *element) {
        int n;

        printf("Choose an element: ");
        fscanf(stdin, "%d", &n);
        if (n == 1) {
            element->type = panel.object1.type;
            element->value = panel.object1.value;
            element->bonus = panel.object1.bonus;
        } else if (n == 2) {
            element->type = panel.object2.type;
            element->value = panel.object2.value;
            element->bonus = panel.object2.bonus;
        } else if (n == 3) {
            element->type = panel.object3.type;
            element->value = panel.object3.value;
            element->bonus = panel.object3.bonus;
        } else
            n = choose(panel, element); //try again --> make it on higher level

        if (element->type == 0)
            n = choose(panel, element); //try again --> make it on higher level

        return n;
    }
    static void locate (Field field, Position *position) {
        printf("Locate an element (x, y): ");
        fscanf(stdin, "%d", &position->x);
        fscanf(stdin, "%d", &position->y);

        while (position->x < 0 || position->x >= field.size || position->y < 0 || position->y >= field.size) {
            printf("Wrong position! Locate an element (x, y) again: ");
            fscanf(stdin, "%d", &position->x);
            fscanf(stdin, "%d", &position->y);
        }

        if (field.space[position->x][position->y] != 0)
            locate(field, position);
    }
    static int switcher (Field field, Position position, Element *element, long *score) {
        char added_successful = FALSE
        ;
        if (element->type == objects::DOT) {
            if (added_successful = (char) add_dot(field, position.x, position.y, element->value))
                *score += points::DOT_POINTS * element->value;
        } else if (element->type == objects::SQUARE2) {
            if (added_successful = (char) add_square(field, position.x, position.y, 2, element->value))
                *score += points::SQUARE2_POINTS * element->value;
        } else if (element->type == objects::SQUARE3) {
            if (added_successful = (char) add_square(field, position.x, position.y, 3, element->value))
                *score += points::SQUARE3_POINTS * element->value;
        } else if (element->type == objects::HLINE2) {
            if (added_successful = (char) add_hline(field, position.x, position.y, 2, element->value))
                *score += points::LINE2_POINTS * element->value;
        } else if (element->type == objects::HLINE3) {
            if (added_successful = (char) add_hline(field, position.x, position.y, 3, element->value))
                *score += points::LINE3_POINTS * element->value;
        } else if (element->type == objects::HLINE4) {
            if (added_successful = (char) add_hline(field, position.x, position.y, 4, element->value))
                *score += points::LINE4_POINTS * element->value;
        } else if (element->type == objects::HLINE5) {
            if (added_successful = (char) add_hline(field, position.x, position.y, 5, element->value))
                *score += points::LINE5_POINTS * element->value;
        } else if (element->type == objects::VLINE2) {
            if (added_successful = (char) add_vline(field, position.x, position.y, 2, element->value))
                *score += points::LINE2_POINTS * element->value;
        } else if (element->type == objects::VLINE3) {
            if (added_successful = (char) add_vline(field, position.x, position.y, 3, element->value))
                *score += points::LINE3_POINTS * element->value;
        } else if (element->type == objects::VLINE4) {
            if (added_successful = (char) add_vline(field, position.x, position.y, 4, element->value))
                *score += points::LINE4_POINTS * element->value;
        } else if (element->type == objects::VLINE5) {
            if (added_successful = (char) add_vline(field, position.x, position.y, 5, element->value))
                *score += points::LINE5_POINTS * element->value;
        } else if (element->type == objects::TRANGLE2) {
            if (added_successful = (char) add_trangle(field, position.x, position.y, 2, element->value))
                *score += points::ANGLE2_POINTS * element->value;
        } else if (element->type == objects::TLANGLE2) {
            if (added_successful = (char) add_tlangle(field, position.x, position.y, 2, element->value))
                *score += points::ANGLE2_POINTS * element->value;
        } else if (element->type == objects::BRANGLE2) {
            if (added_successful = (char) add_brangle(field, position.x, position.y, 2, element->value))
                *score += points::ANGLE2_POINTS * element->value;
        } else if (element->type == objects::BLANGLE2) {
            if (added_successful = (char) add_blangle(field, position.x, position.y, 2, element->value))
                *score += points::ANGLE2_POINTS * element->value;
        } else if (element->type == objects::TRANGLE3) {
            if (added_successful = (char) add_trangle(field, position.x, position.y, 3, element->value))
                *score += points::ANGLE3_POINTS * element->value;
        } else if (element->type == objects::TLANGLE3) {
            if (added_successful = (char) add_tlangle(field, position.x, position.y, 3, element->value))
                *score += points::ANGLE3_POINTS * element->value;
        } else if (element->type == objects::BRANGLE3) {
            if (added_successful = (char) add_brangle(field, position.x, position.y, 3, element->value))
                *score += points::ANGLE3_POINTS * element->value;
        } else if (element->type == objects::BLANGLE3) {
            if (added_successful = (char) add_blangle(field, position.x, position.y, 3, element->value))
                *score += points::ANGLE3_POINTS * element->value;
        }

        if (element->bonus)
            *score += points::BONUS;

        if (added_successful) {
            element->type = 0;
            element->value = 0;
            element->bonus = 0;
            return TRUE;
        } else
            return FALSE; // if error happened
    }
};

#endif //TETRIS1010
