#include "Server.h"

template <class Key, class T>
Server<Key, T>::Server(const std::string& db_path, QObject *parent) : QObject(parent) {
    m_db_path = db_path; //save path to DB
    if (!file_exists(m_db_path)) {
        /* creare new file */
        std::ofstream fl(m_db_path);
        fl.close();
    } else {
        std::ifstream fl(m_db_path);
        Key key;
        T value;
        m_mutex.lock();
        while (std::getline(fl, key) && std::getline(fl, value)) {
            m_db[key] = value;
        }
        m_mutex.unlock();
        fl.close();
    }
}

template <class Key, class T>
Server<Key, T>::~Server() {
    std::ofstream fl(m_db_path);
    m_mutex.lock();
    for (Dictionary::iterator iter = m_db.begin(); iter != m_db.end(); ++iter) {
        fl << iter->first << std::endl;
        fl << iter->second << std::endl;
    }
    m_mutex.unlock();
    fl.close();
}

template <class Key, class T>
void Server<Key, T>::sign_in(const std::__cxx11::string &key) {
    QMutexLocker locker(&m_mutex);
    Dictionary::iterator iter = m_db.find(key);
    if (iter != m_db.end()) {
        emit SignedIn(iter->second);
    } else {
        emit SignedIn("");
    }
}

template <class Key, class T>
void Server<Key, T>::sign_up(const Key& key, const T& value) {
    QMutexLocker locker(&m_mutex);
    Dictionary::iterator iter = m_db.find(key);
    if (iter != m_db.end()) {
        emit SignedUp(false);
    } else {
        m_db[key] = value;
        emit SignedUp(true);
    }
}

template <class Key, class T>
void Server<Key, T>::remove(const Key& key) {
    Dictionary::iterator iter = m_db.find(key);
    if (iter != m_db.end()) {
        m_db.erase(iter);
    }
}

template <class Key, class T>
void Server<Key, T>::get_players(int n) {
    QMutexLocker locker(&m_mutex);
    for (Dictionary::iterator iter = m_db.begin(); iter != m_db.end(); ++iter) {
        emit GetUser(iter->second);
    }
}

template <class Key, class T>
void Server<Key, T>::update(const Key& key, const T& value) {
    QMutexLocker locker(&m_mutex);
    m_db[key] = value;
}

template <class Key, class T>
unsigned int Server<Key, T>::size() const {
    QMutexLocker locker(&m_mutex);
    return m_db.size();
}

template <class Key, class T>
Key Server<Key, T>::find_in_db(const T& value) const {
    QMutexLocker locker(&m_mutex);
    for (Dictionary::iterator iter = m_db.begin(); iter != m_db.end(); ++iter) {
        if (iter->second == value) {
            return iter->first;
        }
    }
    return "";
}

template <class Key, class T>
bool Server<Key, T>::file_exists(const std::__cxx11::string &path) {
    std::ifstream fl(path);
    bool result = fl.is_open();
    fl.close();
    return result;
}
