#include "Tetris10.h"

template <class T>
int field_check_space (Field field, ElementsPanel elpnl) {
    bool space_available = false;

    if (elpnl.object1.type && !space_available)
        space_available = check_element_with_value(elpnl.object1, field);
    if (elpnl.object2.type && !space_available)
        space_available = check_element_with_value(elpnl.object2, field);
    if (elpnl.object3.type && !space_available)
        space_available = check_element_with_value(elpnl.object3, field);

    return (space_available) ? false : true;
}

static char check_element_with_value (Element element, Field field) {
    char space = FALSE;

    if (element.type == objects::DOT) {
        for (int i = 0; i < field.size && !space; i++)
            for (int j = 0; j < field.size && !space; j++) {
                if (field.m_space[i][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::SQUARE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i][j + 1] == 0 &&
                    field.m_space[i + 1][j + 1] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::SQUARE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i][j + 1] == 0 &&
                    field.m_space[i + 1][j + 1] == 0 && field.m_space[i + 2][j] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i + 1][j + 2] == 0 && field.m_space[i + 2][j + 1] == 0 && field.m_space[i + 2][j + 2] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::HLINE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::HLINE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i + 2][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::HLINE4) {
        for (int i = 0; i < field.size - 3 && !space; i++)
            for (int j = 0; j < field.size && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i + 2][j] == 0 &&
                    field.m_space[i + 3][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::HLINE5) {
        for (int i = 0; i < field.size - 4 && !space; i++)
            for (int j = 0; j < field.size && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i + 2][j] == 0 &&
                    field.m_space[i + 3][j] == 0 && field.m_space[i + 4][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::VLINE2) {
        for (int i = 0; i < field.size && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::VLINE3) {
        for (int i = 0; i < field.size && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i][j + 2] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::VLINE4) {
        for (int i = 0; i < field.size && !space; i++)
            for (int j = 0; j < field.size - 3 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i][j + 3] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::VLINE5) {
        for (int i = 0; i < field.size && !space; i++)
            for (int j = 0; j < field.size - 4 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i][j + 3] == 0 && field.m_space[i][j + 4] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::TRANGLE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i + 1][j + 1] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::TLANGLE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i + 1][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::BRANGLE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i + 1][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i + 1][j + 1] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::BLANGLE2) {
        for (int i = 0; i < field.size - 1 && !space; i++)
            for (int j = 0; j < field.size - 1 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 1][j] == 0 && field.m_space[i + 1][j + 1] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::TRANGLE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i + 1][j + 2] == 0 && field.m_space[i + 2][j + 2] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::TLANGLE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i][j + 1] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i + 1][j] == 0 && field.m_space[i + 2][j] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::BRANGLE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i + 2][j] == 0 && field.m_space[i + 2][j + 1] == 0 && field.m_space[i][j + 2] == 0 &&
                    field.m_space[i + 1][j + 2] == 0 && field.m_space[i + 2][j + 2] == 0)
                    space = TRUE;
            }
    } else if (element.type == objects::BLANGLE3) {
        for (int i = 0; i < field.size - 2 && !space; i++)
            for (int j = 0; j < field.size - 2 && !space; j++) {
                if (field.m_space[i][j] == 0 && field.m_space[i + 2][j + 1] == 0 && field.m_space[i + 2][j + 2] == 0 &&
                    field.m_space[i + 1][j] == 0 && field.m_space[i + 2][j] == 0)
                    space = TRUE;
            }
    }

    return space;
}
int field_check_space (Field field, ElementsPanel elpnl) {
    char space_available = FALSE;

    if (elpnl.object1.type && !space_available)
        space_available = check_element_with_value(elpnl.object1, field);
    if (elpnl.object2.type && !space_available)
        space_available = check_element_with_value(elpnl.object2, field);
    if (elpnl.object3.type && !space_available)
        space_available = check_element_with_value(elpnl.object3, field);

    return (space_available) ? FALSE : TRUE;
}

static void field_fill (Field field, int from_x, int from_y, int to_x, int to_y, int value) {
    for (int i = from_x; i < to_x; i++)
        for (int j = from_y; j < to_y; j++)
            field.m_space[i][j] = value;
} // fill field's area with some values
int field_check_combinations (Field field) {
    long collect_points = 0;
    int row_min_value, column_min_value;
    char row_combination = FALSE, column_combination = FALSE;

    //rows:
    for (int i = 0; i < field.size; i++) {
        int j;
        row_min_value = 1984;

        for (j = 0; j < field.size && row_min_value; j++)
            if (field.m_space[i][j] < row_min_value)
                row_min_value = field.m_space[i][j];

        // if there are combination - lower values
        if (j == field.size && row_min_value) {
            row_combination = TRUE;

            for (int j = 0; j < field.size; j++)
                field.m_space[i][j] -= row_min_value;

            collect_points += points::SIMPLE_COMBINATION * row_min_value;
        }
    }

    //columns:
    for (int j = 0; j < field.size; j++) {
        int i;
        column_min_value = 1984;

        for (i = 0; i < field.size && column_min_value; i++)
            if (field.m_space[i][j] < column_min_value)
                column_min_value = field.m_space[i][j];

        // if there are combination - lower values
        if (i == field.size && column_min_value) {
            column_combination = TRUE;

            for (int i = 0; i < field.size; i++)
                field.m_space[i][j] -= column_min_value;

            collect_points += points::SIMPLE_COMBINATION * column_min_value;
        }
    }

    //bonus combinations:
    if (row_combination && column_combination && row_min_value != column_min_value && column_min_value != objects_value::third_level) {
        for (int i = 0; i < field.size; i++)
            for (int j = 0; j < field.size; j++)
                field.m_space[i][j] -= min(row_min_value, column_min_value);

        collect_points += points::BONUS_CROSS * min(row_min_value, column_min_value);
    } else if (row_combination && column_combination) {
        field_fill(field, 0, 0, field.size, field.size, 0);

        collect_points += points::CLEAR_FIELD;
    }

    return collect_points;
}

//void field_output (Field field) {
//    for (int i = 0; i < field.size; i++) {
//        for (int j = 0; j < field.size; j++)
//            printf("%d  ", field.space[i][j]); //--> on higher level
//        putc('\n', stdout);
//    }
//} // displays field
//static void print_switcher (Element element, int number) {
//    printf("object %d: ", number);
//    if (element.type == objects::DOT)
//        printf("dot");
//    else if (element.type == objects::SQUARE2)
//        printf("2-square");
//    else if (element.type == objects::SQUARE3)
//        printf("3-square");
//    else if (element.type == objects::HLINE2)
//        printf("2-horizontal-line");
//    else if (element.type == objects::HLINE3)
//        printf("3-horizontal-line");
//    else if (element.type == objects::HLINE4)
//        printf("4-horizontal-line");
//    else if (element.type == objects::HLINE5)
//        printf("5-horizontal-line");
//    else if (element.type == objects::VLINE2)
//        printf("2-vertical-line");
//    else if (element.type == objects::VLINE3)
//        printf("3-vertical-line");
//    else if (element.type == objects::VLINE4)
//        printf("4-vertical-line");
//    else if (element.type == objects::VLINE5)
//        printf("5-vertical-line");
//    else if (element.type == objects::TRANGLE2)
//        printf("2-top-right-angle");
//    else if (element.type == objects::TLANGLE2)
//        printf("2-top-left-angle");
//    else if (element.type == objects::BRANGLE2)
//        printf("2-bottom-right-angle");
//    else if (element.type == objects::BLANGLE2)
//        printf("2-bottom-left-angle");
//    else if (element.type == objects::TRANGLE3)
//        printf("3-top-right-angle");
//    else if (element.type == objects::TLANGLE3)
//        printf("3-top-left-angle");
//    else if (element.type == objects::BRANGLE3)
//        printf("3-bottom-right-angle");
//    else if (element.type == objects::BLANGLE3)
//        printf("3-bottom-left-angle");
//    //values:
//    if (element.value == objects_value::first_level)
//        printf(" [first-level-element] ");
//    else if (element.value == objects_value::second_level)
//        printf(" [second-level-element] ");
//    else if (element.value == objects_value::third_level)
//        printf(" [third-level-element] ");
//    //bonuses:
//    if (element.bonus == bonus_value::bonus_1)
//        printf(" {bonus_1-element} ");
//
//    putc('\n', stdout);
//}
//void elpnl_output (ElementsPanel panel) {
//    if (panel.object1.type)
//        print_switcher(panel.object1, 1);
//    if (panel.object2.type)
//        print_switcher(panel.object2, 2);
//    if (panel.object2.type)
//        print_switcher(panel.object3, 3);
//} // displays elements on the screen

int Tetris10::add_dot (int x, int y, int value) {
    m_field.m_space[x][y] = value;
    return true;
}
int Tetris10::add_hline (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x][y + i] != 0)
            return false;

    for (int i = 0; i < size; i++)
        m_field.m_space[x][y + i] = value;

    return true;
}
int Tetris10::add_vline (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x + i][y] != 0)
            return false;

    for (int i = 0; i < size; i++)
        m_field.m_space[x + i][y] = value;

    return true; //all right
}
int Tetris10::add_square (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size() || y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            if (m_field.m_space[x + i][y + j] != 0)
                return false;
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            m_field.m_space[x + i][y + j] = value;

    return true;
}
int Tetris10::add_trangle (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size() || y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x][y + i] != 0)
            return false;
    for (int i = 1; i < size; i++)
        if (m_field.m_space[x + i][y + size - 1] != 0)
            return false;
    for (int i = 0; i < size; i++)
        m_field.m_space[x][y + i] = value;
    for (int i = 1; i < size; i++)
        m_field.m_space[x + i][y + size - 1] = value;

    return true;
}
int Tetris10::add_tlangle (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size() || y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x][y + i] != 0)
            return false;
    for (int i = 1; i < size; i++)
        if (m_field.m_space[x + i][y] != 0)
            return false;
    for (int i = 0; i < size; i++)
        m_field.m_space[x][y + i] = value;
    for (int i = 1; i < size; i++)
        m_field.m_space[x + i][y] = value;
    return true;
}
int Tetris10::add_brangle (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size() || y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x + size - 1][y + i] != 0)
            return false;
    for (int i = 0; i < size - 1; i++)
        if (m_field.m_space[x + i][y + size - 1] != 0)
            return false;
    for (int i = 0; i < size; i++)
        m_field.m_space[x + size - 1][y + i] = value;
    for (int i = 0; i < size - 1; i++)
        m_field.m_space[x + i][y + size - 1] = value;

    return true;
}
int Tetris10::add_blangle (int x, int y, int size, int value) {
    //protect from segmentation fault
    if (x + size > m_field.size() || y + size > m_field.size())
        return false;
    //check whether all position are empty:
    for (int i = 0; i < size; i++)
        if (m_field.m_space[x + size - 1][y + i] != 0)
            return false;
    for (int i = 0; i < size - 1; i++)
        if (m_field.m_space[x + i][y] != 0)
            return false;
    for (int i = 0; i < size; i++)
        m_field.m_space[x + size - 1][y + i] = value;
    for (int i = 0; i < size - 1; i++)
        m_field.m_space[x + i][y] = value;

    return true;
}
