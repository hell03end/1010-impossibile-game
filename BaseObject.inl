/**
 * @file Object.inl
 * @category basic object class
 *
 * @brief Class of object, which is basic to all other classes.
 *
 * @details
 * Contains fields and their methods to avoid exception generation in constructors.
 * Also contains virtual methods to serialize object data.
 * @todo: store type of object.
 *
 * @author hell03end
 * @version 0.11
 * @date 11-Oct-16 : 07-Dec-16
 */

#ifndef BASE_OBJECT_INL
#define BASE_OBJECT_INL

#include "ErrMessages.h"
#include <typeinfo>
#include <string>

template <class T>
class BaseObject {
public:
    BaseObject(void) {
        m_err_amount = 0;
        m_err = EXIT_SUCCESS; //means no errors
        m_type = typeid(*this).name(); // store type name //@undone <--
    }
    virtual ~BaseObject(void) { /* Nothing */ }

    void set_error(const int errnum) {
        m_err = errnum;
        m_err_amount++;
    }
    inline int get_last_error(void) const { return m_err; }
    unsigned int get_err_amount(void) {
        unsigned int result = m_err_amount;
        /* restore normal values */
        m_err_amount = 0;
        m_err = EXIT_SUCCESS;
        return result;
    }
    inline unsigned int show_err_amount(void) const { return m_err_amount; }

    inline virtual T serialize(void) const = 0; //transform object data into standard representation type
    inline virtual bool deserialize(const T& message) = 0; //create object from standard representation type
private:
    unsigned int m_err_amount;
    int m_err; //store type of error
    std::string m_type; //store type of object
};

#endif //BASE_OBJECT_INL
