#include "ui_InfoWindow.h"
#include "InfoWindow.h"

InfoWindow::InfoWindow(QWidget *parent) : QDialog(parent) {
    ui = new Ui::InfoWindow();
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked, this, &InfoWindow::close);
}

InfoWindow::~InfoWindow() {
    delete ui;
}
