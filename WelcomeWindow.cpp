#include "WelcomeWindow.h"
#include "ui_WelcomeWindow.h"

WelcomeWindow::WelcomeWindow(QWidget* parent) : QWidget(parent) {
    ui = new Ui::WelcomeWindow; //create ui
    ui->setupUi(this);

    /* FIELDS VALIDATION: */
    /* create regular expressions to check input */
    QRegExp is_input_email("([a-zA-Z\.-_]+)@([a-zA-Z\.-]+)(\.[a-zA-Z\.]+)");
    QRegExp is_input_login("[a-zA-Zа-яА-Я0-9.-_]{3,20}");
    QRegExp is_input_pass("[a-zA-Zа-яА-Я0-9.-_!@#$%^&*()=+№;:?\"]{7,30}");
    /* create input validators */
    ui->lineEdit_login->setValidator(new QRegExpValidator(is_input_login, this));
    ui->lineEdit_password->setValidator(new QRegExpValidator(is_input_pass, this));
    ui->lineEdit_email_reg->setValidator(new QRegExpValidator(is_input_email, this));
    ui->lineEdit_login_reg->setValidator(new QRegExpValidator(is_input_login, this));
    ui->lineEdit_password_reg->setValidator(new QRegExpValidator(is_input_pass, this));
    ui->lineEdit_confirm_password_reg->setValidator(new QRegExpValidator(is_input_pass, this));
    /* connect each action with input fields with inspection */
    connect(ui->lineEdit_login, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_sign_in);
    connect(ui->lineEdit_password, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_sign_in);
    connect(ui->lineEdit_login_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_password_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_confirm_password_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_email_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->checkBox_accept, &QCheckBox::clicked,
            this, &WelcomeWindow::button_enable_register);

    /* buttons' actions */
    connect(ui->pushButton_exit, &QPushButton::clicked,
            this, &WelcomeWindow::close);
    connect(ui->pushButton_sign_up, &QPushButton::clicked,
            this, &WelcomeWindow::go_to_registration);
    connect(ui->pushButton_back, &QPushButton::clicked,
            this, &WelcomeWindow::go_to_welcome);
    connect(ui->pushButton_play_offline, &QPushButton::clicked,
            this, &WelcomeWindow::play_online);
    connect(ui->pushButton_register, &QPushButton::clicked,
            this, &WelcomeWindow::sign_up);
    connect(ui->pushButton_sign_in, &QPushButton::clicked,
            this, &WelcomeWindow::sign_in);
    connect(ui->pushButton_about, &QPushButton::clicked,
            this, &WelcomeWindow::call_about);
}

WelcomeWindow::~WelcomeWindow() {
    delete ui;
}

void WelcomeWindow::signed_in(int code, char key) {
    if (code == RTRNSCCSS) {
        restart(); // clear all information
        play_online(true);
        return;
    } else if (code == WRGDAT) {
        show_message('c', "Incorrect format of login/password",
                     "Login >= 3 & <= 20 symbols, without spaces.\n"
                     "Password >=7 & <=30 symbols, at least 1 digit or symbol and 1 letter.\n");
    } else if (code == WRGDAT1) {
        show_message('c', "User not exists!",
                     "Check login/password or register first. You also can play offline.");
        ui->lineEdit_password->clear();
    } else if (code == ACCDEN) {
        show_message('c', "Wrong login/password!",
                     "Try another credentials or Sign Up first. You also can play offline.");
        ui->lineEdit_password->clear();
    } else if (code == SRVRNAVLBL) {
        show_message('c', "Server isn't available!", "But you can play offline instead.");
    } else if (code == SRVROVERLD) {
        show_message('c', "Server is overloaded!",
                     "You can wait for a few minutes or play offline instead.");
    }
    this->setEnabled(true);
    this->show();
}

void WelcomeWindow::signed_up(int code, char key) {
    if (code == RTRNSCCSS) {
        restart(); // clear all information
        play_online(true);
    } else {
        if (code == WRGDAT) {
            if (key == 'l') {
                show_message('c', "User with such login exists!", "Please, enter another login.");
                ui->lineEdit_login_reg->clear();
                ui->lineEdit_login_reg->setPlaceholderText("LOGIN");
            } else if (key == 'e') {
                show_message('c', "User with such email exists!", "Please, enter another username.");
                ui->lineEdit_email_reg->clear();
                ui->lineEdit_email_reg->setPlaceholderText("EMAIL");
            } else {
                show_message('c', "User with such credentials exists!", "Please, enter another credentials.");
                ui->lineEdit_login_reg->clear();
                ui->lineEdit_login_reg->setPlaceholderText("LOGIN");
                ui->lineEdit_email_reg->clear();
                ui->lineEdit_email_reg->setPlaceholderText("EMAIL");
            }
        } else if (code == WRGDAT1) {
            //@undone:
            show_message('c', "Incorrect format of user data!", "Please, enter data again.",
                       "Login >= 3 & <= 20 symbols, without spaces.\n"
                       "Password >=7 & <=30 symbols, at least 1 digit or symbol and 1 letter.\n");
            ui->lineEdit_password_reg->clear();
            ui->lineEdit_confirm_password_reg->clear();
        } else if (code == SRVRNAVLBL) {
            show_message('c', "Server isn't available!", "But you can play offline instead.");
        } else if (code == SRVROVERLD) {
            show_message('c', "Server is overloaded!", "You can wait for a few minutes or play offline instead.");
        }
    }
}

void WelcomeWindow::set_geometry(const QRect& size) {
    this->setGeometry(size);
}

void WelcomeWindow::sign_up() {
    if (ui->lineEdit_password_reg->text() != ui->lineEdit_confirm_password_reg->text()) {
        show_message('c', "Passwords don't match!", "Enter password again.");
        ui->lineEdit_confirm_password_reg->clear();
        ui->lineEdit_password_reg->clear(); //forget passwords
    } else {
        /* send signal with player data trying to register */
        emit Geometry(this->geometry());
        emit SignUp(ui->lineEdit_login_reg->text().toStdString(),
                    ui->lineEdit_password_reg->text().toStdString(),
                    ui->lineEdit_email_reg->text().toStdString());
    }
}

void WelcomeWindow::sign_in() {
    /* send signal with player data trying to log in */
    emit Geometry(this->geometry());
    emit SignIn(ui->lineEdit_login->text().toStdString(),
                ui->lineEdit_password->text().toStdString());
}

void WelcomeWindow::call_about() {
    emit CallAbout();
}

void WelcomeWindow::play_online(bool online) {
    this->setEnabled(false);
    this->hide(); // hide current window
    emit Geometry(this->geometry());
    emit PlayOnline(online);
}

void WelcomeWindow::go_to_registration() {
    if (ui->lineEdit_password_reg->text().toStdString().empty()) {
        ui->lineEdit_password_reg->setText(ui->lineEdit_password->text());
        ui->lineEdit_confirm_password_reg->setText(ui->lineEdit_password->text());
    }
    ui->lineEdit_password->clear(); //forget password
    if (ui->lineEdit_login_reg->text().toStdString().empty()) {
        ui->lineEdit_login_reg->setText(ui->lineEdit_login->text());
    }
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_registration));
}

void WelcomeWindow::go_to_welcome() {
    ui->lineEdit_password_reg->clear(); //forget passwords:
    ui->lineEdit_confirm_password_reg->clear();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_welcome));
}

void WelcomeWindow::restart() {
    ui->lineEdit_login->clear();
    ui->lineEdit_password->clear();
    ui->lineEdit_login_reg->clear();
    ui->lineEdit_email_reg->clear();
    ui->lineEdit_password_reg->clear();
    ui->lineEdit_confirm_password_reg->clear();
    ui->checkBox_accept->setChecked(true);
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_welcome));
}

void WelcomeWindow::button_enable_sign_in() {
    ui->pushButton_sign_in->setEnabled(ui->lineEdit_login->hasAcceptableInput() &&
                                       ui->lineEdit_password->hasAcceptableInput());
}

void WelcomeWindow::button_enable_register() {
    ui->pushButton_register->setEnabled(ui->lineEdit_login_reg->hasAcceptableInput() &&
                                        ui->lineEdit_password_reg->hasAcceptableInput() &&
                                        ui->lineEdit_confirm_password_reg->hasAcceptableInput() &&
                                        ui->lineEdit_email_reg->hasAcceptableInput() &&
                                        ui->checkBox_accept->isChecked());
}

/* show window (message box) with some info */
void WelcomeWindow::show_message(char type, const std::__cxx11::string& title,
                                 const std::__cxx11::string& body,
                                 const std::__cxx11::string& detals) {
    QMessageBox window;
    if (type == 'q' || type == 'Q') {
        window.setIcon(QMessageBox::Question);
    } else if (type == 'i' || type == 'I') {
        window.setIcon(QMessageBox::Information);
    } else if (type == 'w' || type == 'W') {
        window.setIcon(QMessageBox::Warning);
    } else if (type == 'c' || type == 'C') {
        window.setIcon(QMessageBox::Critical);
    }
    window.setDefaultButton(QMessageBox::Ok); //create only one button
    window.setText("<b>"+QString::fromStdString(title)+"</b>"); //show title message
    if (body != "") {
        window.setInformativeText(QString::fromStdString(body)); //add informative text
    }
    if (detals != "") {
        window.setDetailedText(QString::fromStdString(detals)); //add detailed text
    }
    window.exec();
}
